mergeInto(LibraryManager.library, {

  VKInit: function() {
    console.log("VK.init started");
    VK.init(function() {
        SendMessage('VKWEBWrapper', 'VKInitCallback');
    });
  },
  
  VKUserInfo: function() {
    VK.api('users.get', {"fields": "photo_200, photo_100"}, function(data) {SendMessage('VKWEBWrapper', 'VKUserInfoCallback', JSON.stringify(data.response));});
  },
  
  ExecString: function(str) {
    eval(str);
  },

});