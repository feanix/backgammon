﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Assets.src.scripts.Core;
using Assets.src.scripts.Core.Analysis;
using Assets.src.scripts.Core.Constants;
using DG.Tweening;

public class GameBoard : MonoBehaviour {

	private const int PART_SIZE = 6;

    [SerializeField] private List<Transform> _boardParts;
    [SerializeField] private Transform _player1Side;
    [SerializeField] private Transform _player2Side;
	[SerializeField] private Transform _checkersContainer;

	public MoveEvent onMove = new MoveEvent();
	public BoolEvent onMoveStarted = new BoolEvent();

	private Board _board;

	private List<ColumnDisplay> _columnsIndex;
	private Dictionary<CheckersColumn, ColumnDisplay> _columns;
    private Dictionary<Checker, CheckerDisplay> _checkers;
    private Dictionary<PlayerType, Transform> _playerSides;

    private Player _currentPlayer;
    private List<CheckerMove> _moves;
	private CheckersColumn _fromColumn;
	private CheckersColumn _toColumn;

    public void init(Board board) {
		int number = 0;
		int partNumber = 0;
		CheckersColumn column;

        clear();

        _board = board;

        for (int i = 0; i < Board.LENGTH; i ++) {
			if (number >= PART_SIZE) {
				++partNumber;
				number = 0;
			}
			++number;
			column = board.getColumn(null, i);
            addColumn(column, _boardParts[partNumber], new Vector3(number - PART_SIZE / 2 - 0.5f, 0, 0), "Column " + (i + 1).ToString());
		}
        addPlayerColumns(PlayerType.PLAYER_1);
        addPlayerColumns(PlayerType.PLAYER_2);
    }

    private void addPlayerColumns(PlayerType playerType) {
        Player player;
        player = _board.getPlayer(playerType);
        addColumn(player.finished.column, _playerSides[playerType], new Vector3(-6.95f, 0, 0), "Column Finished " + (int)playerType);
        addColumn(player.banned.column, _playerSides[playerType], new Vector3(0, 0, 0), "Column Banned " + (int)playerType);
    }

    private void addColumn(CheckersColumn column, Transform parent, Vector3 position, string name) {
        ColumnDisplay columnDisplay;
        Checker checker;
        CheckerDisplay checkerDisplay;
        uint columnLength;
        columnDisplay = Instantiate(Catalogue.resourceHelper.ColumnPrefab, parent, false);
        columnDisplay.transform.localPosition = position;
        columnDisplay.init(column);
		columnDisplay.name = name;
		columnDisplay.onClick.AddListener(columnClickHandler);
		_columns.Add(column, columnDisplay);
        _columnsIndex.Add(columnDisplay);
        columnLength = column.length;
        for (int i = 0; i < columnLength; i ++) {
            checker = column.getChecker(i);
            checkerDisplay = Instantiate(Catalogue.resourceHelper.CheckerPrefab, _checkersContainer, false);
            checkerDisplay.init(checker);
            _checkers.Add(checker, checkerDisplay);
        }

		update(true);
    }

	public void update(bool immidiate = false) {
		CheckersColumn column;
		ColumnDisplay columnDisplay;
		CheckerDisplay checkerDisplay;
		Checker checker;
		Vector3 position;
		foreach (KeyValuePair<Checker, CheckerDisplay> keyValuePair in _checkers) {
			checker = keyValuePair.Key;
			column = checker.column;
			columnDisplay = _columns[column];
			checkerDisplay = keyValuePair.Value;
			position = columnDisplay.getCheckerPosition((int)column.length - checker.position - 1);
			if (immidiate)
				checkerDisplay.transform.position = position;
			else
				checkerDisplay.transform.DOMove(position, Catalogue.MOVE_SPEED);
		}
	}

	public bool undo() {
		disableColumns();
		if (_fromColumn != null) {
			_fromColumn = null;
			_toColumn = null;
			activateColumns();
			onMoveStarted.Invoke(false);
			return true;
		}
		return false;
	}

	private void columnClickHandler(CheckersColumn column) {
		CheckerMove move = null;
		List<CheckersColumn> toColumns = new List<CheckersColumn>();

        disableColumns();

        if (_fromColumn == null) {
			_fromColumn = column;
            if (_moves != Catalogue.FREE_MODE) {
                foreach (CheckerMove checkerMove in _moves) {
                    if (checkerMove.fromColumn.column == column)
                        toColumns.Add(checkerMove.toColumn.column);
                }

                foreach (CheckersColumn checkersColumn in toColumns) {
                    _columns[checkersColumn].mouseEnabled = true;
                }
            } else {
                activateAllColumns(_currentPlayer, false);
            }
            onMoveStarted.Invoke(true);
		} else if (_toColumn == null) {
			_toColumn = column;
            if (_moves != Catalogue.FREE_MODE) {
                foreach (CheckerMove checkerMove in _moves) {
                    if ((checkerMove.fromColumn.column == _fromColumn) && (checkerMove.toColumn.column == _toColumn)) {
                        move = checkerMove;
                        break;
                    }
                }
            } else {
                move = new CheckerMove(_currentPlayer, _fromColumn, _toColumn);
            }
            clearMoves();
            onMoveStarted.Invoke(false);
            onMove.Invoke(move);
        } else {
			throw new Exception("Unexpected click in a column.");
		}
	}

	public void moveRequired(Player player, List<CheckerMove> moves) {
        _currentPlayer = player;
        _moves = moves;
        disableColumns();

        if (moves != Catalogue.FREE_MODE)
		    activateColumns();
        else
            activateAllColumns(player, true);
    }

    private void activateAllColumns(Player player, bool from) {
        PlayerColumn column;
        for (int i = 0; i < Board.LENGTH; i ++) {
            column = player.getColumn(i);
            if (!from || (column.occupant == _currentPlayer))
                _columns[column.column].mouseEnabled = true;
        }
        if (!from) {
            _columns[player.banned.column].mouseEnabled = true;
            _columns[player.finished.column].mouseEnabled = true;
        }
    }

    public void activateColumns() {
		PlayerColumn playerColumn;
		CheckersColumn column;
		foreach (CheckerMove move in _moves) {
			playerColumn = move.fromColumn;
			column = playerColumn.column;
			_columns[column].mouseEnabled = true;
		}
	}

	private void disableColumns() {
		foreach (ColumnDisplay columnDisplay in _columnsIndex)
			columnDisplay.mouseEnabled = false;
	}

	private void clearMoves() {
		_fromColumn = null;
		_toColumn = null;
		_moves = null;
        _currentPlayer = null;
    }

    public void Awake() {
		_playerSides = new Dictionary<PlayerType, Transform>();
        _playerSides.Add(PlayerType.PLAYER_1, _player1Side);
        _playerSides.Add(PlayerType.PLAYER_2, _player2Side);
    }

    public void clear() {
		if (_columnsIndex != null) {
			foreach (ColumnDisplay column in _columnsIndex)
				column.dispose();
		}
		if (_checkers != null) {
			foreach (KeyValuePair<Checker, CheckerDisplay> keyValuePair in _checkers)
                keyValuePair.Value.dispose();
		}
        _columnsIndex = new List<ColumnDisplay>();
		_columns = new Dictionary<CheckersColumn, ColumnDisplay>();
		_checkers = new Dictionary<Checker, CheckerDisplay>();
		_fromColumn = null;
		_toColumn = null;
		_moves = null;
        _currentPlayer = null;
    }

}
