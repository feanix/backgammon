﻿using System.Collections;
using Assets.src.scripts.Utills.DTO;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.src.scripts.Managers {

	public class PlayerManager {

		private string _firstName;
		private string _lastName;
		private string _fullName;
		private string _pictureURL;
		private Texture2D _pictureTex;
		private int _points;
		private int _games;

		public readonly UnityEvent onUpdate = new UnityEvent();
		public readonly UnityEvent onPictureUpdate = new UnityEvent();

		public PlayerManager() {}

		public PlayerManager(PlayerData data) {
			init(data);
		}

		public void init(PlayerData data) {
			firstName = data.firstName;
			lastName = data.lastName;
			games = data.games;
			points = data.points;
			setPicture(data.pictureURL);
		}

		void setPicture(string URL) {
			_pictureURL = URL;
			_pictureTex = null;
			updateTexture();
			Catalogue.StartCoroutine(loadTexture(_pictureURL));
		}

		IEnumerator loadTexture(string URL) {
			using (WWW www = new WWW(URL)) {
				yield return www;
				_pictureTex = www.texture;
				updateTexture();
			}
		}

		public Texture2D texture {
			get { return _pictureTex; }
		}

		public string fullName {
			get { return _fullName; }
		}

		public string firstName {
			get { return _firstName; }
			set {
				_firstName = value;
				_fullName = _firstName + " " + _lastName;
				update();
			}
		}

		public string lastName {
			get { return _lastName; }
			set {
				_lastName = value;
				_fullName = _firstName + " " + _lastName;
				update();
			}
		}

		public int points {
			get { return _points; }
			set {
				_points = value;
				update();
			}
		}

		public int games {
			get { return _games; }
			set {
				_games = value;
				update();
			}
		}

		private void update() {
			onUpdate.Invoke();
		}

		private void updateTexture() {
			onPictureUpdate.Invoke();
		}

	}

}
