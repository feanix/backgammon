﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UserInfoWindow : WindowDefault {

	public static UserInfoWindow _instance;

	private static bool _enabled = true;

	public static UserInfoWindow instance {
		get { return _instance; }
	}

	public new static bool enabled {
		set {
			_enabled = value;
			update();
		}
		get { return _enabled; }
	}

	private static bool _gameStarted = false;
	public static void setGameStarted(bool value) {
		_gameStarted = value;
		update();
	}

	void Awake() {
		_instance = this;
		update();
	}

	private static void update() {
		if (_instance != null)
			_instance.gameObject.SetActive(_enabled && !_gameStarted);
	}

}
