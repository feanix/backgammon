﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using Assets.src.scripts.Core.Constants;
using System.Collections;

public class NewGameWindow : WindowDefault {

	[SerializeField] private Button _closeButton;
	[SerializeField] private Button _dragonButton;
	[SerializeField] private Button _classicButton;

	private Action<Layout> _callback;

	public void init(Action<Layout> callback) {
		_callback = callback;
		onClick(_dragonButton, GameType.DEMO);
		onClick(_classicButton, GameType.BASIC);
	}

	private void onClick(Button button, Layout gameType) {
		UnityAction listener = () => {
			closeWindow();
			if (_callback != null)
				_callback.Invoke(gameType);
		};

		button.onClick.AddListener(listener);
	}

	public void Awake() {
		_closeButton.gameObject.SetActive(false);
		onClick(_closeButton, null);
	}

	private void closeWindow() {
		_closeButton.onClick.RemoveAllListeners();
		_dragonButton.onClick.RemoveAllListeners();
		_classicButton.onClick.RemoveAllListeners();
		Destroy(gameObject);
	}

}
