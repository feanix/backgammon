﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class WindowsSingleton : MonoBehaviour {

	[SerializeField] private GameObject _curtain;

	private static Transform windowsContainer;
	private static GameObject curtain;
	private static List<WindowDefault> _windows = new List<WindowDefault>();

	public void Awake() {
		windowsContainer = gameObject.transform;
		curtain = _curtain;
	}

	private static void windowClosed(WindowDefault window) {
		_windows.Remove(window);
		curtain.SetActive(_windows.Count > 0);
	}

	public static T addWindow<T>(T windowPrefab) where T : WindowDefault {
		T result;
		result = Instantiate(windowPrefab.gameObject, windowsContainer, false).GetComponent<T>();
		_windows.Add(result);
		curtain.SetActive(true);
		result.onDestroy.AddListener(windowClosed);
		return result;
	}

}
