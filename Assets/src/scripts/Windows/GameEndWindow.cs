﻿using UnityEngine;
using System.Collections;
using Assets.src.scripts.Managers;
using UnityEngine.UI;

public class GameEndWindow : MonoBehaviour {

	[SerializeField] private Text _userName;
	[SerializeField] private GameObject _winPart;
	[SerializeField] private GameObject _losePart;
	[SerializeField] private Button _exitButton;

	void Start() {
		hide();
		_exitButton.onClick.AddListener(exitHandler);
	}

	private void exitHandler() {
		hide();
	}

	public void hide() {
		gameObject.SetActive(false);
	}

	public void show(bool win) {
		_winPart.SetActive(win);
		_losePart.SetActive(!win);
		_userName.text = Catalogue.appUser.fullName;
		gameObject.SetActive(true);
	}

	void OnDestroy() {
		_exitButton.onClick.RemoveListener(exitHandler);
	}

}
