﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreenWindow : WindowDefault {

	[SerializeField] private Text _progressField;
	[SerializeField] private LocalizedText _noteField;
	[SerializeField] private Image _progressBar;

	public float progress {
		set {
			_progressField.text = Math.Floor(value * 100) + "%";
			_progressBar.fillAmount = value;
		}
	}

	public string note {
		set { _noteField.reference = value; }
	}

}
