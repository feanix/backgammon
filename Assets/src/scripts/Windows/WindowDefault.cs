﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class WindowDefault : MonoBehaviour {

	[SerializeField] private bool _curtain = true;

	public readonly WindowEvent onDestroy = new WindowEvent();

	public bool curtain {
		get { return _curtain; }
	}

	private void OnDestroy() {
		onDestroy.Invoke(this);
	}

}
