﻿using System;
using System.Collections;
using UnityEngine;

public class Stage : MonoBehaviour {

	private static MainGame _game;

	private static LoadingScreenWindow _loadingScreen;

	public static Stage instance {
		get { return Catalogue.stage; }
	}

	public static LoadingScreenWindow loadingScreen {
		get { return _loadingScreen; }
	}

	public static void loadingComplete() {
		instance.StartCoroutine(instance.ExecuteAfterTime(1, loadingCompleteHandler));
	}

	private static void loadingCompleteHandler() {
		_loadingScreen.gameObject.SetActive(false);
		UserInfoWindow.enabled = true;
		_game = MonoBehaviour.Instantiate(Catalogue.resourceHelper.gameDisplayPrefab, null, false);
	}

	IEnumerator ExecuteAfterTime(float time, Action callback) {
		yield return new WaitForSeconds(time);
		callback.Invoke();
	}

	void Start() {
		gameObject.name = "Stage";
		_loadingScreen = Instantiate(Catalogue.resourceHelper.loadingScreenWindowPrefab, Catalogue.resourceHelper.topCanvas, false);
		loadingScreen.progress = 0;
		UserInfoWindow.enabled = false;
		Catalogue.init(this);
	}

}

