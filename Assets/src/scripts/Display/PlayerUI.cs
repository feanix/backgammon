﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerUI : MonoBehaviour {

	[SerializeField] private GameObject _captionContainer;
	[SerializeField] private Text _caption;
	[SerializeField] public DiceDisplay dice;

	public bool visible {
		set { _captionContainer.SetActive(value); }
	}

	public string playerName {
		set { _caption.text = value; }
	}

}
