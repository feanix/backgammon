﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.src.scripts.Core;
using Assets.src.scripts.Core.Constants;

public class CheckerDisplay : MonoBehaviour {

	[SerializeField] private GameObject _rendererBlack;
	[SerializeField] private GameObject _rendererBlackDragon;
	[SerializeField] private GameObject _rendererBlackTurtle;
	[SerializeField] private GameObject _rendererWhite;
	[SerializeField] private GameObject _rendererWhiteDragon;
	[SerializeField] private GameObject _rendererWhiteTurtle;

	private Dictionary<PlayerType, Dictionary<CheckerType, GameObject>> _rendererIndex;

	private Checker _checker;
	private GameObject _renderer;

	public void init(Checker checker) {
		_renderer = Instantiate(_rendererIndex[checker.player.type][checker.type], transform, false);
	}

	public CheckerType type {
		get { return _checker.type; }
	}

	public Checker checker {
		get { return _checker; }
	}

	public PlayerType playerType {
		get { return _checker.player.type; }
	}

    public new GameObject renderer {
        get { return _renderer; }
    }

	public void Awake() {
		_rendererIndex = new Dictionary<PlayerType, Dictionary<CheckerType, GameObject>>();
		_rendererIndex.Add(PlayerType.PLAYER_1, new Dictionary<CheckerType, GameObject>());
		_rendererIndex[PlayerType.PLAYER_1].Add(CheckerType.BASIC, _rendererWhite);
		_rendererIndex[PlayerType.PLAYER_1].Add(CheckerType.DRAGON, _rendererWhiteDragon);
		_rendererIndex[PlayerType.PLAYER_1].Add(CheckerType.TURTLE, _rendererWhiteTurtle);
		_rendererIndex.Add(PlayerType.PLAYER_2, new Dictionary<CheckerType, GameObject>());
		_rendererIndex[PlayerType.PLAYER_2].Add(CheckerType.BASIC, _rendererBlack);
		_rendererIndex[PlayerType.PLAYER_2].Add(CheckerType.DRAGON, _rendererBlackDragon);
		_rendererIndex[PlayerType.PLAYER_2].Add(CheckerType.TURTLE, _rendererBlackTurtle);
	}

	public void OnDestroy() {
		_checker = null;
	}

    public void dispose() {
        Destroy(gameObject);
    }

}
