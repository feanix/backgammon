﻿using UnityEngine;
using Assets.src.scripts.Core;
using System.Collections;
using System.Collections.Generic;
using Assets.src.scripts.Core.Constants;
using UnityEngine.EventSystems;

public class ColumnDisplay : MonoBehaviour, IPointerClickHandler {

	public ColumnClickEvent onClick = new ColumnClickEvent();

	[SerializeField] private BoxCollider _mouseTarget;
	[SerializeField] private GameObject _selector;

	private bool _mouseEnabled;
    private CheckersColumn _column;

	public void init(CheckersColumn column) {
		_column = column;
	}

    public Vector3 getCheckerPosition(int number) {
        Vector3 result;
        result = new Vector3(0, 0.5f*number, - number/10f);
        result = transform.TransformPoint(result);
        return result;
    }

	public CheckersColumn column {
		get { return _column; }
	}

	public bool mouseEnabled {
		get { return _mouseEnabled; }
		set {
			_mouseEnabled = value;
			_mouseTarget.gameObject.SetActive(value);
			_selector.SetActive(value);
		}
	}

	public void OnPointerClick(PointerEventData eventData) {
		onClick.Invoke(column);
	}

	public void Awake() {
		mouseEnabled = false;
	}

    public void OnDestroy() {
		_column = null;
	}

    public void dispose() {
        Destroy(gameObject);
    }

}
