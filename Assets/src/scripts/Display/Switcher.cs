﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Switcher : MonoBehaviour {

	[SerializeField] private Button _buttonON;
	[SerializeField] private Button _buttonOFF;
	[SerializeField] private bool _enabled = true;
	[SerializeField] private BoolEvent onChange = new BoolEvent();

	public new bool enabled {
		get { return _enabled; }
		private set {
			_enabled = value;
			update();
			onChange.Invoke(_enabled);
		}
	}

	private void enable() {
		enabled = true;
	}

	private void disable() {
		enabled = false;
	}

	private void update() {
		_buttonON.gameObject.SetActive(_enabled);
		_buttonOFF.gameObject.SetActive(!_enabled);
	}

	private void Awake() {
		_buttonON.onClick.AddListener(disable);
		_buttonOFF.onClick.AddListener(enable);
		update();
	}

	private void OnDestroy() {
		_buttonON.onClick.RemoveAllListeners();
		_buttonOFF.onClick.RemoveAllListeners();
	}

}
