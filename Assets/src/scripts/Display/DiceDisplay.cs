﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Assets.src.scripts.Core.Dices;
using DG.Tweening.Core;
using DG.Tweening;
using DG;
using System;
using System.Collections;

public class DiceDisplay : MonoBehaviour {

	private const int ROLL_NUMBER = 10;

	[SerializeField] private Sprite _number1;
	[SerializeField] private Sprite _number2;
	[SerializeField] private Sprite _number3;
	[SerializeField] private Sprite _number4;
	[SerializeField] private Sprite _number5;
	[SerializeField] private Sprite _number6;

	[SerializeField] private Image _dice1;
	[SerializeField] private Image _dice2;

	Action _callback = null;

	private uint _ready = 0;

	public void throwDice(Throw dices, Action callback) {
		if (_ready != 0)
			throw new Exception("Dices are still in motion.");
		_ready = dices.numberOfDices;
		_dice1.gameObject.SetActive(true);
		_callback = callback;
		StartCoroutine(rollDice(_dice1, dices.dice));
		if (dices.numberOfDices > 1) {
			_dice2.gameObject.SetActive(true);
			StartCoroutine(rollDice(_dice2, (dices as ThrowTwo).dice2));
		} else {
			_dice2.gameObject.SetActive(false);
		}
	}

	IEnumerator rollDice(Image dice, uint result, int step = 0) {
		float time;
		uint number;
		Sprite sprite = null;

		time = UnityEngine.Random.Range(0.05f, 0.05f);

		if (step < ROLL_NUMBER)
			number = (uint)UnityEngine.Random.Range(1, 7);
		else
			number = result;

		yield return new WaitForSeconds(time);

		switch (number) {
			case 1:
				sprite = _number1;
				break;
			case 2:
				sprite = _number2;
				break;
			case 3:
				sprite = _number3;
				break;
			case 4:
				sprite = _number4;
				break;
			case 5:
				sprite = _number5;
				break;
			case 6:
				sprite = _number6;
				break;
		}
		dice.sprite = sprite;

		if (step < ROLL_NUMBER)
			StartCoroutine(rollDice(dice, result, ++step));
		else
			diceComplete();
	}

	private void diceComplete() {
		--_ready;
		if ((_ready == 0) && (_callback != null))
			_callback.Invoke();
	}

	public void hide() {
		_dice1.gameObject.SetActive(false);
		_dice2.gameObject.SetActive(false);
	}

	void Awake() {
		hide();
	}


}
