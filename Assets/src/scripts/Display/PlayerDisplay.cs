﻿using UnityEngine;
using Assets.src.scripts.Managers;
using UnityEngine.UI;

public class PlayerDisplay : MonoBehaviour {

	public enum PlayerOrigin {
		AppUser = 0,
		CurrentPlayer = 1,
		Custom = 2,
	}

	[SerializeField] private PlayerOrigin _user = PlayerOrigin.CurrentPlayer;

	[SerializeField] private Text _firstName;
	[SerializeField] private Text _lastName;
	[SerializeField] private Text _fullName;
	[SerializeField] private Text _gamesTotal;
	[SerializeField] private Text _pointsTotal;
	[SerializeField] private Image _avatar;

	private PlayerManager _player;

	void Start() {
		if (_user == PlayerOrigin.AppUser) {
			Catalogue.onAppUserChange.AddListener(autoUpdate);
			autoUpdate();
		} else if (_user == PlayerOrigin.CurrentPlayer) {
			Catalogue.onCurrentPlayerChange.AddListener(autoUpdate);
			autoUpdate();
		}
	}

	public PlayerManager player {
		set {
			if (_player != null) {
				_player.onUpdate.RemoveListener(onPlayerDataUpdate);
				_player.onPictureUpdate.RemoveListener(onPlayerPictureUpdate);
			}
			_player = value;
			if (_player != null) {
				_player.onUpdate.AddListener(onPlayerDataUpdate);
				_player.onPictureUpdate.AddListener(onPlayerPictureUpdate);
			}
			if (_player != null)
				update();
		}
	}

	private void autoUpdate() {
		if (_user == PlayerOrigin.AppUser) {
			player = Catalogue.appUser;
		} else if (_user == PlayerOrigin.CurrentPlayer) {
			player = Catalogue.currentPlayer;
		}
	}

	private void update() {
		onPlayerDataUpdate();
		onPlayerPictureUpdate();
	}

	private void onPlayerDataUpdate() {
		if (_firstName != null)
			_firstName.text = _player.firstName;
		if (_lastName != null)
			_lastName.text = _player.lastName;
		if (_fullName != null)
			_fullName.text = _player.fullName;
		if (_pointsTotal != null)
			_pointsTotal.text = _player.points.ToString();
		if (_gamesTotal != null)
			_gamesTotal.text = _player.games.ToString();
	}

	private void onPlayerPictureUpdate() {
		if (_avatar == null)
			return;
		Texture2D texture = _player.texture;
		if (texture == null)
			return;
		Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
		_avatar.sprite = sprite;
	}

	public void OnDestroy() {
		Catalogue.onAppUserChange.RemoveListener(autoUpdate);
		Catalogue.onCurrentPlayerChange.RemoveListener(autoUpdate);
		player = null;
	}

}
