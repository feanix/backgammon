﻿using System;
using UnityEngine;
using Assets.src.scripts.Core;
using Assets.src.scripts.Core.Dices;
using Assets.src.scripts.Core.Constants;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using Assets.src.scripts.Core.Analysis;

public class MainGame : MonoBehaviour {

    public PlayerEvent onThrowRequired = new PlayerEvent();
	public PlayerEvent onCurrentPlayer = new PlayerEvent();
	public BoolEvent onUndoStatus = new BoolEvent();
	public RollEvent onDouble = new RollEvent();
	public PlayerEvent onWin = new PlayerEvent();

	[SerializeField] private GameBoard _boardDisplay;

    private Board _board;

	private Action<PlayerType> _throwCallback;

    private Player _currentPlayer;
    private Dictionary<Player, Throw> _firstThrow;
	private Dictionary<PlayerType, Player> _players;
	private Player _player1;
	private Player _player2;
	private Player _startPlayer;

	private Throw _lastDices;

	private Analyzer _analyzer = null;

	public Throw throwDices(Throw roll, bool forced = false) {
		Player player = roll.player;
        if (currentPlayer != player)
            throw new Exception("Dices were thrown by a wrong player.");

		Debug.Log("Throw Dices: " + player.type + " (" + roll + ")");
        if (!_firstThrow.ContainsKey(player))
            addFirstThrow(player, roll);
		else if (!player.ready) {
			player.initThrow(roll);
			placeCheckers();
		} else {
			if (!forced && roll.isDouble && currentPlayer.retrieveAvailable) {
				onDouble.Invoke(roll);
			} else {
				makeMove(roll);
			}
		}
        return roll;
    }

	public void retrieve(Throw roll) {
		roll.player.retrieve(roll as ThrowTwo);
		_boardDisplay.update();
		endMove();
	}

	private void onMoveStartedHandler(bool moveStarted) {
		onUndoStatus.Invoke(moveStarted || ((_analyzer != null) && _analyzer.moveApplied));
	}

	public void undo() {
		if (!_boardDisplay.undo()) {
			cleanAnalysis();
			_board.tempMode = false;
			_boardDisplay.update();
			makeMove(_lastDices);
		}
	}

	private void makeMove(Throw roll = null, CheckerMove move = null) {
		List<CheckerMove> moves;
		if ((_analyzer == null) && (roll != null)) {
			_lastDices = roll;
			_analyzer = new Analyzer(roll);
			_board.tempMode = true;
		}
		if (move != null) {
			move.apply();
			_boardDisplay.update();
            if (_analyzer != null)
			    _analyzer.applyMove(move);
		}
        if (_freeMode) {
            freeMoveRequest();
        } else if (_analyzer != null) {
            moves = _analyzer.getMoves();
            if (moves.Count > 0) {
                _boardDisplay.moveRequired(currentPlayer, moves);
                onUndoStatus.Invoke(move != null);
            } else {
                _board.applyChanges();
                _lastDices = null;
                endMove();
            }
        }
	}

	private void endMove(bool nextPlayer = true) {
		if (currentPlayer.complete) {
			onWin.Invoke(currentPlayer, 0);
		} else {
            if (nextPlayer)
			    switchPlayer();
			cleanAnalysis();
			onThrowRequired.Invoke(currentPlayer, 2);
		}
	}

	private void onMoveHandler(CheckerMove move) {
		Debug.Log("Start move");
		makeMove(null, move);
	}

	private void cleanAnalysis() {
		if (_analyzer != null) {
			_analyzer.dispose();
			_analyzer = null;
			onUndoStatus.Invoke(false);
		}
	}

	private void placeCheckers() {
		if (_board.ready) {
			startGame();
			return;
		}
		if (!currentPlayer.ready) {
			Debug.Log(currentPlayer.type + " PlaceCheckers");
			onThrowRequired.Invoke(currentPlayer, currentPlayer.requiredThrow);
		}  else {
			switchPlayer();
			placeCheckers();
		}
    }

	private void startGame() {
		setCurrentPlayer(_startPlayer);
		Debug.Log(currentPlayer.type + " Start Game");
		_boardDisplay.init(_board);
		onThrowRequired.Invoke(currentPlayer, 2);
	}

	private void addFirstThrow(Player player, Throw result) {
        _firstThrow.Add(player, result);
        if (_firstThrow.Count == 2) {
            if (_firstThrow[_player1].total != _firstThrow[_player2].total) {
                if (_firstThrow[_player1] < _firstThrow[_player2]) {
					_startPlayer = _player2;
                } else {
					_startPlayer = _player1;
				}
				setCurrentPlayer(_startPlayer);
                placeCheckers();
            } else {
                _firstThrow = new Dictionary<Player, Throw>();
                setCurrentPlayer(_player1);
                onThrowRequired.Invoke(currentPlayer, 1);
            }
        } else {
            switchPlayer();
            onThrowRequired.Invoke(currentPlayer, 1);
        }
    }

    private Player currentPlayer {
        get { return _currentPlayer; }
    }

    private void setCurrentPlayer(Player player) {
        _currentPlayer = player;
	    Catalogue.currentPlayerType = player.type;
        onCurrentPlayer.Invoke(player, 0);
    }

    private void switchPlayer() {
        if (_currentPlayer.type == PlayerType.PLAYER_1)
            setCurrentPlayer(_player2);
        else
            setCurrentPlayer(_player1);
    }

    public void init(Layout gameType) {
		clear();
		_board = new Board(gameType);
		_player1 = _board.getPlayer(PlayerType.PLAYER_1);
		_player2 = _board.getPlayer(PlayerType.PLAYER_2);
		_players.Add(_player1.type, _player1);
		_players.Add(_player2.type, _player2);
		setCurrentPlayer(_player1);
        onThrowRequired.Invoke(_player1, 1);
	}

	public void exitGame() {
		clear();
	}

	private void clear() {
        _freeMode = false;
        _boardDisplay.clear();
		cleanAnalysis();
		if (_board != null)
			_board.dispose();
		_board = null;
        _firstThrow = new Dictionary<Player, Throw>();
        _currentPlayer = null;
		_players = new Dictionary<PlayerType, Player>();
		_player1 = null;
		_player2 = null;
		_startPlayer = null;
	}

	public void Awake() {
		_boardDisplay.onMove.AddListener(onMoveHandler);
		_boardDisplay.onMoveStarted.AddListener(onMoveStartedHandler);
	}

    private void freeMoveRequest() {
        _boardDisplay.moveRequired(currentPlayer, Catalogue.FREE_MODE);
    }

    private bool _freeMode = false;
	public void Update() {
		PlayerColumn toColumn;
		PlayerColumn fromColumn;
		CheckerMove move;
		bool done;
		if (Application.isEditor && Input.GetKeyUp("x")) {
			done = false;
			toColumn = currentPlayer.getColumn(Board.LENGTH - 1);
			while (!done) {
				done = true;
				foreach (Checker checker in currentPlayer.checkers) {
					if ((checker.column != currentPlayer.finished.column) && (checker.column != toColumn.column)) {
						fromColumn = currentPlayer.getColumn(checker.column);
						move = new CheckerMove(fromColumn, toColumn);
						move.apply();
						done = false;
					}
				}
			}
			_boardDisplay.update();
		}
        if (Application.isEditor && Input.GetKeyUp("f")) {
            if (!_freeMode) {
                _boardDisplay.undo();
                undo();
                freeMoveRequest();
            } else {
                endMove(false);
            }
            _freeMode = !_freeMode;
        }
    }

}
