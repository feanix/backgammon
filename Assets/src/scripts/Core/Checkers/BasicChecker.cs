﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.src.scripts.Core.Constants;

namespace Assets.src.scripts.Core.Checkers {

    public class BasicChecker : Checker {

        public BasicChecker(Player player) : base(player, CheckerType.BASIC) {}

        /*override public bool availableMove(PlayerColumn column) {
            Player toPlayer;
            toPlayer = column.occupant;
            if ((toPlayer != null) && (toPlayer != player))
                return false;
            return true;
        }*/

        override public bool banEnabled {
            get { return true; }
        }

    }

}
