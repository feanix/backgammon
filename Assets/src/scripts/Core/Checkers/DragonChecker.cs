﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.src.scripts.Core.Constants;

namespace Assets.src.scripts.Core.Checkers {

    public class DragonChecker : Checker {

        public DragonChecker(Player player) : base(player, CheckerType.DRAGON) { }

        /*override public bool availableMove(PlayerColumn column) {
            Player toPlayer;
            toPlayer = column.occupant;
            if ((toPlayer != null) && (toPlayer != player)) {
                if (column.column.length > 1)
                    return false;
                if (!column.column.topChecker.banEnabled)
                    return false;
            }
            return true;
        }*/

        override public bool banEnabled {
            get { return true; }
        }

    }

}
