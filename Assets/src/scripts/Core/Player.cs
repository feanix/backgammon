﻿using System;
using System.Collections.Generic;
using Assets.src.scripts.Core.Analysis;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Interfaces;
using Assets.src.scripts.Core.Checkers;
using Assets.src.scripts.Core.Dices;
using Assets.src.scripts.Managers;

namespace Assets.src.scripts.Core {

    public class Player : IDispose {

        private Board _board;
        private PlayerType _type;
		private List<Checker> _checkers;
	    private PlayerColumn _banned;
	    private PlayerColumn _finished;
		private List<PlayerColumn> _columns;
		private Dictionary<CheckersColumn, PlayerColumn> _columnsIndex;
		private Layout _layout;
		private PlayerColumn _headColumn;

        public Player(Board board, PlayerType type) {
            _board = board;
			_layout = board.type.instance;
			_type = type;
			init();
        }

	    private void init() {
		    clear();
        }

        public List<CheckerType> initThrow(Throw dices) {
            List<CheckerType> result;
            result = _layout.throwDice(dices);
            if (_layout.ready)
                initLayout();
            return result;
		}

		public bool ready {
			get { return _layout.ready; }
		}

		public uint requiredThrow {
			get { return _layout.requiredThrow; }
		}

		private void initLayout() {
			Checker checker = null;
			List<CheckerType> layout = _layout.layout;
			foreach (CheckerType checkerType in layout) {
				switch (checkerType) {
					case CheckerType.BASIC:
						checker = new BasicChecker(this);
						break;
					case CheckerType.DRAGON:
						checker = new DragonChecker(this);
						break;
					case CheckerType.TURTLE:
						throw new Exception("CheckerType \"Turtle\" has not been added to the game yet.");
					default:
						throw new Exception("Unsupported CheckerType \"" + checkerType.ToString() + "\".");
				}
				_checkers.Add(checker);
				_columns[PlayerColumn.HEAD_COLUMN].column.addChecker(checker);
			}
		}

		private void placeChecker(Checker checker, int column) {
            CheckersColumn checkersColumn = getColumn(column).column;
            checkersColumn.addChecker(checker);
        }

        internal void setColumn(CheckersColumn column) {
            int columnsCounter = 0;
            PlayerColumn playerColumn;
            playerColumn = new PlayerColumn(this, ColumnType.PLAY, column);
			if (playerColumn.head)
				_headColumn = playerColumn;
            _columns[playerColumn.index] = playerColumn;
			if (_columnsIndex.ContainsKey(column))
				_columnsIndex[column] = playerColumn;
			else
				_columnsIndex.Add(column, playerColumn);
            foreach (PlayerColumn tempColumn in _columns)
                if (tempColumn != null)
                    ++columnsCounter;
            if ((columnsCounter == Board.LENGTH) && _layout.ready)
                initLayout();
        }

        public PlayerColumn getColumn(int index) {
            if ((index >= Board.LENGTH) || (index < 0))
                throw new Exception("Column index \"" + index + "\" is out of board.");
            return _columns[index];
        }

		public PlayerColumn getColumn(CheckersColumn column) {
			return _columnsIndex[column];
		}

		public bool retrieveAvailable {
			get { return (_banned.column.length > 0) && ((_headColumn.occupant == this) || (_headColumn.occupant == null)); }
		}

		public List<CheckerMove> retrieve(ThrowTwo roll, bool apply = true) {
			List<CheckerMove> result;
			CheckersColumn bannedColumn;
			CheckerMove move;
			if (!roll.isDouble)
				throw new Exception("Checkers can be retrieved only by double.");
			if (!retrieveAvailable)
				throw new Exception("There are no checkers in the banned column.");
			result = new List<CheckerMove>();
			bannedColumn = _banned.column;
			for (int i = 0; i < roll.dice; i++)
				if (bannedColumn.length > 0) {
					move = new CheckerMove(_banned, _headColumn);
					result.Add(move);
					if (apply)
						move.apply();
				}
			return result;
		}

		public bool complete {
			get {
				return (finished.column.length == _checkers.Count);
			}
		}

	    public void ban(Checker checker, bool top = true) {
			banned.column.addCheckerAt(checker, top ? 0 : -1);
	    }

        public bool isFinishing {
            get { return _layout.finish(this); }
        }

	    public PlayerColumn banned {
		    get { return _banned; }
	    }

	    public PlayerColumn finished {
		    get { return _finished; }
	    }

	    public List<Checker> checkers {
		    get { return _checkers; }
	    }

        public PlayerType type {
            get { return _type; }
        }

	    public Board board {
		    get { return _board; }
	    }

		internal bool tempMode {
			set {
				_banned.tempMode = value;
				_finished.tempMode = value;
			}
		}

		public string name {
			get { return type.ToString(); }
		}

		internal void applyChanges(bool playMode = true) {
			_banned.applyChanges(playMode);
			_finished.applyChanges(playMode);
		}

		private void clear() {
	        Checker tempChecker;

			_columnsIndex = new Dictionary<CheckersColumn, PlayerColumn>();

			_headColumn = null;
			if (_columns != null) {
                foreach (PlayerColumn playerColumn in _columns)
                    if (playerColumn != null)
                        playerColumn.dispose();
            }

            _columns = new List<PlayerColumn>();
			for (int i = 0; i < Board.LENGTH; i++)
				_columns.Add(null);
			_banned = new PlayerColumn(this, ColumnType.BANNED, new CheckersColumn(CheckersColumn.BANNED_INDEX));
	        _finished = new PlayerColumn(this, ColumnType.FINISHED, new CheckersColumn(CheckersColumn.FINISHED_INDEX));

			if (_checkers != null) {
			    for (int i = 0; i < _checkers.Count; i++) {
				    tempChecker = _checkers[i];
				    if (tempChecker == null)
						continue;
					tempChecker.dispose();
					_checkers[i] = null;
			    }
	        }
			_checkers = new List<Checker>();
        }

        public void dispose() {
			clear();
			if (_layout != null) {
				_layout.dispose();
				_layout = null;
			}
	        _checkers = null;
            _board = null;
	        _banned = null;
	        _finished = null;
        }

	}

}
