﻿namespace Assets.src.scripts.Core.Constants {

    public enum CheckerType : int {
        BASIC = 1,
        DRAGON = 2,
        TURTLE = 3
    }

}
