﻿using System;
using System.Collections.Generic;
using Assets.src.scripts.Core.Dices;
using Assets.src.scripts.Core.Interfaces;
using Assets.src.scripts.Core.Rules;

namespace Assets.src.scripts.Core.Constants {

	public class GameType {

		public static readonly Layout BASIC = Basic.getInstance();
		public static readonly Layout DEMO = Demo.getInstance();

	}

	public class Basic : Layout {

		private static int _hachCode = new Random().Next(int.MaxValue);
		private const int LAYOUT_ID = 1;
		private static readonly List<uint> THROWS_REQUIRED = new List<uint>{ };

        private Dictionary<CheckerType, Rule> _rules = new Dictionary<CheckerType, Rule> {
            {CheckerType.BASIC, RuleBasic.instance}
        };

        private Basic() : base(LAYOUT_ID) {
            addCheckers(CheckerType.BASIC, 15);
		}

		override protected List<uint> _throwsRequired {
			get { return THROWS_REQUIRED; }
		}

        override public string name {
            get { return "Классические правила"; }
        }

        public static Basic getInstance() {
            return new Basic();
        }

		override public Layout instance {
			get { return Basic.getInstance(); }
		}

		override protected int hashCode { get { return _hachCode; } }

        override public Dictionary<CheckerType, Rule> rules {
            get { return _rules; }
        }

    }

    public class Demo : Layout {

		private static int _hachCode = new Random().Next(int.MaxValue);
		private const int LAYOUT_ID = 2;
		private static readonly List<uint> THROWS_REQUIRED = new List<uint> { 2 };

        private Dictionary<CheckerType, Rule> _rules = new Dictionary<CheckerType, Rule> {
            {CheckerType.BASIC, RuleBasic.instance},
            {CheckerType.DRAGON, RuleDragon.instance}
        };

        private Demo() : base(LAYOUT_ID) {
            addCheckers(CheckerType.BASIC, 14);
            addCheckers(CheckerType.DRAGON, 1);
		}

        public List<CheckerType> throwDice(ThrowTwo dices) {
			base.throwDice(dices);
			CheckerType dragon;
            int position;
			dragon = _layout[_layout.Count - 1];
            position = (int)(dices.dice + dices.dice2);
			_layout.Remove(dragon);
			_layout.Insert(position - 1, dragon);
			return currentLayout;
        }

        public override List<CheckerType> throwDice(Throw dices) {
            List<CheckerType> result;
            if (!(dices is ThrowTwo))
                throw new Exception("Two dices are supposed to be thrown.");
            result = throwDice(dices as ThrowTwo);
            return result;
        }

        override protected List<uint> _throwsRequired {
			get { return THROWS_REQUIRED; }
		}

        override public string name {
            get { return "Игра с одним драконом"; }
        }

		public static Demo getInstance() {
			return new Demo();
		}

		override public Layout instance {
			get { return Demo.getInstance(); }
		}

		override protected int hashCode { get { return _hachCode; } }

        override public Dictionary<CheckerType, Rule> rules {
            get { return _rules; }
        }

    }

    public abstract class Layout : IDispose {

		private int _id;
		private uint _throw = 0;
		protected List<CheckerType> _layout;


		protected Layout(int id) {
			clear();
			_id = id;
		}

        public virtual bool finish(Player player) {
            bool result;
            uint number = 0;
            CheckersColumn column;
            for (int i = Board.LENGTH - Dice.MAXIMUM; i < Board.LENGTH; i ++) {
                column = player.getColumn(i).column;
                if ((column.length > 0) && (column.topChecker.player == player))
                    number += column.length;
            }
            column = player.finished.column;
            number += column.length;
            result = number == _layout.Count;
            return result;
        }

        protected void addCheckers(CheckerType type, uint number) {
            for (int i = 0; i < number; i ++) {
                _layout.Add(type);
            }
        }

		protected abstract List<uint> _throwsRequired { get; }
		public List<uint> throwsRequired {
			get {
				if (_throwsRequired == null)
					throw new Exception("Required throws are not set for the Layout.");
				return _throwsRequired.GetRange(0, _throwsRequired.Count);
			}
		}

        public abstract Dictionary<CheckerType, Rule> rules { get; }

        public virtual List<CheckerType> throwDice(Throw dices) {
			if (ready)
				throw new Exception("All initial throws required have already been made.");
			if (dices.numberOfDices != throwsRequired[(int)_throw])
				throw new Exception("Wrong number of dices was thrown (" + dices.numberOfDices + " instead of " + throwsRequired[(int)_throw] + ").");
			++_throw;
			return currentLayout;
		}

		public List<CheckerType> layout {
            get {
				if (!ready)
					throw new Exception("Only " + _throw + " of " + throwsRequired.Count + " initial throws have been made.");
				return _layout.GetRange(0, _layout.Count);
			}
        }

		public List<CheckerType> currentLayout {
			get {
				return _layout.GetRange(0, _layout.Count);
			}
		}

		public uint requiredThrow {
			get {
				if (ready)
					return 0;
				return throwsRequired[(int)_throw];
			}
		}

		public uint throwsMade {
			get { return _throw; }
		}

		public bool ready {
			get { return _throw >= throwsRequired.Count; }
		}

		public abstract Layout instance { get; }

		override public bool Equals(Object layout) {
			if (!(layout is Layout))
				return false;
			if ((layout as Layout).id != id)
				return false;
			return true;
		}

		protected abstract int hashCode { get; }
		override public int GetHashCode() {
			return hashCode;
		}

        public abstract string name { get; }

        public static bool operator == (Layout layout1, Layout layout2) {
			return layout1.Equals(layout2);
		}

		public static bool operator !=(Layout layout1, Layout layout2) {
			return !layout1.Equals(layout2);
		}

		protected int id {
			get { return _id; }
		}

		private void clear() {
			_layout = new List<CheckerType>();
		}

		public void dispose() {
			_throw = 0;
			_id = 0;
		}

	}

}
