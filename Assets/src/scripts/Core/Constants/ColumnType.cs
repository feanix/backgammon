﻿namespace Assets.src.scripts.Core.Constants {

    public enum ColumnType : int {
        PLAY = 1,
        BANNED = 2,
        FINISHED = 3
    }

}
