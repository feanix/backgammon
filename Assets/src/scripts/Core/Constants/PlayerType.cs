﻿namespace Assets.src.scripts.Core.Constants {

    public enum PlayerType : int {
        PLAYER_1 = 1,
        PLAYER_2 = 2
    }

}
