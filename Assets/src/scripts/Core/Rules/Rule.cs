﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Analysis;
using Assets.src.scripts.Core;

namespace Assets.src.scripts.Core.Rules {

    public abstract class Rule {

        private CheckerType _checkerType;

        public Rule(CheckerType checkerType) {
            _checkerType = checkerType;
        }

        public abstract bool check(CheckerMove move);
        public abstract List<CheckerMove> apply(CheckerMove move);

        public CheckerType checkerType {
            get { return _checkerType; }
        }

    }

}
