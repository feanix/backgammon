﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Analysis;

namespace Assets.src.scripts.Core.Rules {

    class RuleBasic : Rule {

        private static RuleBasic _instance = new RuleBasic();

        private RuleBasic():base(CheckerType.BASIC) {}

        override public bool check(CheckerMove move) {
            bool result = true;
            Checker checker;
            Checker toChecker;
            checker = move.checker;
            if (checker.type != checkerType)
                return false;
            toChecker = move.toColumn.column.topChecker;
            if ((toChecker != null) && (toChecker.player.type != checker.player.type))
                return false;
                //throw new Exception("Checker " + checker.type + " does not match the rool type " + checkerType + ".");
            return result;
        }

        public override List<CheckerMove> apply(CheckerMove move) {
            move.toColumn.column.addChecker(move.checker);
            return null;
        }

        public static RuleBasic instance {
            get { return _instance; }
        }

    }

}
