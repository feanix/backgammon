﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Analysis;

namespace Assets.src.scripts.Core.Rules {

    class RuleDragon : Rule {

        private static RuleDragon _instance = new RuleDragon();

        private RuleDragon():base(CheckerType.DRAGON) {}

        override public bool check(CheckerMove move) {
            bool result = true;
            Checker checker;
            Checker toChecker;
            CheckersColumn toColumn;
            checker = move.checker;
            if (checker.type != checkerType)
                return false;
            toColumn = move.toColumn.column;
            toChecker = toColumn.topChecker;
            if ((toChecker != null) && (toChecker.player.type != checker.player.type) && ((toColumn.length > 1) || (toChecker.ownerColumn.head)))
                return false;
            return result;
        }

        public override List<CheckerMove> apply(CheckerMove move) {
            List<CheckerMove> result = null;
            CheckerMove newMove;
            Checker checker;
            Checker toChecker;
            CheckersColumn toColumn;
            checker = move.checker;
            toColumn = move.toColumn.column;
            toChecker = toColumn.topChecker;
            if (move.toColumn.type == ColumnType.BANNED) {
                toColumn.addCheckerAt(checker, -1);
            } else if ((toChecker != null) && (toChecker.player.type != checker.player.type)) {
                move.disableChecker();
                result = new List<CheckerMove>();
                newMove = new CheckerMove(move.toColumn, toChecker.player.banned, move);
                newMove.apply();
                result.Add(newMove);
                toColumn.addChecker(checker);
            } else {
                toColumn.addChecker(checker);
            }
            return result;
        }

        public static RuleDragon instance {
            get { return _instance; }
        }

    }

}
