﻿using System;
using System.Collections.Generic;
using Assets.src.scripts.Core.Interfaces;
using Assets.src.scripts.Core.Constants;

namespace Assets.src.scripts.Core {

    public class CheckersColumn : IDispose {

        public const int BANNED_INDEX = -1;
        public const int FINISHED_INDEX = -2;

        private int _index;
		private List<Checker> _checkers;
		private List<Checker> _tempCheckers;
		private bool _tempMode;

        public CheckersColumn(int index) {
            clear();
            _index = index;
        }

        public uint length {
            get {
                uint result;
                result = (uint)checkers.Count;
                return result;
            }
        }

        public int getPlayerIndex(Player player) {
			if (_index == FINISHED_INDEX)
				return Board.LENGTH;
            return Board.getColumnByPlayer(player, _index);
        }

        public int getPosition(Checker checker) {
			return checkers.IndexOf(checker);
		}

		public Checker getChecker(int number) {
			if ((number < 0) || (number >= length))
				throw new Exception("Checker number " + number + " is out of range.");
			return checkers[number];
		}

	    public void addChecker(Checker checker) {
			addCheckerAt(checker, 0);
	    }

	    public void addCheckerAt(Checker checker, int index) {
		    if (index < 0)
			    index = checkers.Count + index + 1;
            if (checker.column != null)
                checker.column.removeChecker(checker);
            checker.column = this;
            checkers.Insert(index, checker);
        }

        private void removeChecker(Checker checker) {
            if (checkers.Count == 0)
                throw new Exception("The column \"" + _index + "\" does not contain any checkers.");
            if (checker != checkers[0])
                throw new Exception("The checker on top of the column \"" + _index + "\" does not match with one you try to move.");
            if (checker.column == this)
                checker.column = null;
            checkers.RemoveAt(0);
        }

        public Checker topChecker {
            get {
                if (checkers.Count == 0)
                    return null;
                return checkers[0];
            }
        }

		internal bool tempMode {
			get { return _tempMode; }
			set {
				if (_tempMode == value)
					return;
				_tempMode = value;
				if (_tempMode)
					_tempCheckers = _checkers.GetRange(0, _checkers.Count);
				else {
					_tempCheckers = null;
					foreach (Checker checker in checkers)
						checker.column = this;
				}
			}
		}

		public void applyChanges(bool playMode = true) {
			if (!tempMode)
				throw new Exception("This column is not in the TempMode.");
			_checkers = _tempCheckers;
			tempMode = false;
			if (!playMode) {
				tempMode = true;
			}
		}

		private List<Checker> checkers {
			get {
				if (_tempCheckers != null)
					return _tempCheckers;
				return _checkers;
			}
		}

        private void clear() {
			_tempMode = false;
			_tempCheckers = null;
            _checkers = new List<Checker>();
        }

        public void dispose() {
            clear();
            _index = -1;
            _checkers = null;
        }

    }

}
