﻿using System;
using System.Collections.Generic;
using Assets.src.scripts.Core.Interfaces;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Rules;

namespace Assets.src.scripts.Core.Analysis {

	public class CheckerMove : IDispose {

		private PlayerColumn _from;
		private PlayerColumn _to;
		private Checker _checker;
        private Player _player;
        private Rule _rule;
		private int _length;
        private Analyzer _analyzer;

        public CheckerMove(PlayerColumn fromColumn, PlayerColumn toColumn) {
            init(fromColumn, toColumn, null);
        }

        public CheckerMove(Player player, CheckersColumn fromColumn, CheckersColumn toColumn) {
            PlayerColumn fromPlayerColumn;
            PlayerColumn toPlayerColumn;
            fromPlayerColumn = player.getColumn(fromColumn);
            toPlayerColumn = player.getColumn(toColumn);
            init(fromPlayerColumn, toPlayerColumn, null);
        }

        internal CheckerMove(PlayerColumn fromColumn, PlayerColumn toColumn, CheckerMove source) {
            init(fromColumn, toColumn, source._analyzer);
        }

        internal CheckerMove(PlayerColumn fromColumn, PlayerColumn toColumn, Analyzer analyzer) {
            init(fromColumn, toColumn, analyzer);
		}

        private void init(PlayerColumn fromColumn, PlayerColumn toColumn, Analyzer analyzer) {
            clear();
            _from = fromColumn;
            _to = toColumn;
            _analyzer = analyzer;
            if (_from == null)
                throw new Exception("FromColumn must not be null.");
            if (_to == null)
                throw new Exception("ToColumn must not be null.");
            _checker = fromColumn.column.topChecker;
            if (_checker == null)
                throw new Exception("Checker must be different from null.");
            if (_checker != null)
                _player = _checker.player;
            _rule = checker.rule;
            _length = _to.index - _from.index;
        }

        public void disableChecker() {
            if (_analyzer != null)
                _analyzer.disableChecker(_checker);
        }

        public int length {
			get { return _length; }
		}

        public List<CheckerMove> apply() {
            List<CheckerMove> result = null;
            if (available) {
                result = _rule.apply(this);
                //toColumn.column.addChecker(checker);
            } else
                throw new Exception("Attempt to make an unavailable move.");
            return result;
        }

		public bool available {
            get {
                /*bool result;
                if (checker == null)
                    return false;
                result = checker.availableMove(toColumn);
                return result;*/
                return _rule.check(this);
            }
		}

		public Checker checker {
			get { return _checker; }
		}

		public PlayerColumn fromColumn {
			get { return _from; }
		}

		public PlayerColumn toColumn {
			get { return _to; }
		}

		public Player player {
			get {
                if (_player == null)
                    _player = _checker.player;
                return checker.player;
            }
		}

		private void clear() {
            _checker = null;
            _from = null;
            _to = null;
            _player = null;
		}

		public void dispose() {
			clear();
		}

	}

}

