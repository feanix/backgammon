﻿using System;
using System.Collections.Generic;
using Assets.src.scripts.Core.Interfaces;

namespace Assets.src.scripts.Core.Analysis {

	public class MoveSequence : IDispose {

		private CheckerMove _move1;
        private CheckerMove _move2;
        private CheckerMove _move3;
        private CheckerMove _move4;

        public MoveSequence(CheckerMove move1, CheckerMove move2, CheckerMove move3 = null, CheckerMove move4 = null) {
			clear();
            _move1 = move1;
            _move2 = move2;
            _move3 = move3;
            _move4 = move4;
            if (move1.player != move2.player)
				throw new Exception("You are trying to move different player's checkers at a time.");
            if ((move3 != null) && (move3.player != move1.player))
                throw new Exception("You are trying to move different player's checkers at a time.");
            if ((move4 != null) && (move4.player != move1.player))
                throw new Exception("You are trying to move different player's checkers at a time.");
        }

        public Player player {
			get { return _move1.player; }
		}

		public CheckerMove move1 {
			get { return _move1; }
		}

        public CheckerMove move2 {
            get { return _move2; }
        }

        public CheckerMove move3 {
            get { return _move3; }
        }

        public CheckerMove move4 {
            get { return _move4; }
        }

        private void clear() {
            if (_move1 != null) {
                _move1.dispose();
                _move1 = null;
            }
            if (_move2 != null) {
                _move2.dispose();
                _move2 = null;
            }
            if (_move3 != null) {
                _move3.dispose();
                _move3 = null;
            }
            if (_move4 != null) {
                _move4.dispose();
                _move4 = null;
            }
		}

		public void dispose() {
			clear();
		}

	}

}

