﻿using System;
using System.Collections.Generic;
using Assets.src.scripts.Core.Interfaces;
using Assets.src.scripts.Core.Dices;

namespace Assets.src.scripts.Core.Analysis {

	public class Analyzer : IDispose {

        private Player _player;
		private List<uint> _dices;
		private List<PlayerColumn> _heads;
		private bool _movesApplied;
        private List<Checker> _disabledCheckers;

        public Analyzer(Throw dices) {
			clear();
			_player = dices.player;
			_dices = dices.moves.GetRange(0, dices.moves.Count);
			_dices.Sort();
		}

		public void applyMove(CheckerMove move) {
			int length = move.length;
			bool moveFinded = false;

			if (move.fromColumn.head)
				_heads.Add(move.fromColumn);

			for (int i = 0; i < _dices.Count; i++) {
				if (_dices[i] >= length) {
					_dices.RemoveAt(i);
					moveFinded = true;
					break;
				}
			}
			_movesApplied = true;

			if (!moveFinded)
				throw new Exception("The Analyzer does not contain such a Move.");
		}

        public List<CheckerMove> getMoves() {
            List<CheckerMove> result;
            PlayerColumn fromColumn;
            PlayerColumn toColumn;
            List<uint> dices;
            int toIndex;
            CheckerMove move;
            List<PlayerColumn> finishingColumns;
            List<PlayerColumn> lastColumns;
            PlayerColumn finishColumn;
            bool finishingColumn;
            bool lastColumn;

            dices = new List<uint>();
            foreach (uint dice in _dices)
                if (!dices.Contains(dice))
                    dices.Add(dice);

            result = new List<CheckerMove>();
            finishingColumns = new List<PlayerColumn>();
            lastColumns = new List<PlayerColumn>();
            for (int i = 0; i < Board.LENGTH; i ++) {
                fromColumn = _player.getColumn(i);
				if (_heads.Contains(fromColumn))
					continue;
                if (fromColumn.occupant != _player)
                    continue;
                finishingColumn = false;
                lastColumn = false;
                foreach (int dice in dices) {
                    toIndex = fromColumn.index + dice;
                    if (toIndex >= Board.LENGTH) {
                        if (toIndex == Board.LENGTH)
                            finishingColumn = true;
                        else
                            lastColumn = true;
                        continue;
                    }
                    toColumn = _player.getColumn(toIndex);
                    move = new CheckerMove(fromColumn, toColumn, this);
                    checkAndAddMove(result, move);
                }
                if (finishingColumn)
                    finishingColumns.Add(fromColumn);
                if (lastColumn)
                    lastColumns.Add(fromColumn);
            }

            if (_player.isFinishing) {
                finishColumn = _player.finished;
                foreach (PlayerColumn finishing in finishingColumns) {
                    move = new CheckerMove(finishing, finishColumn, this);
                    checkAndAddMove(result, move);
                }
                if (result.Count == 0) {
                    foreach (PlayerColumn finishing in lastColumns) {
                        move = new CheckerMove(finishing, finishColumn, this);
                        checkAndAddMove(result, move);
                    }
                }
            }
            return result;
        }

        private void checkAndAddMove(List<CheckerMove> list, CheckerMove move) {
            if (move.available)
                list.Add(move);
        }

        internal bool isEnabled(Checker checker) {
            return !_disabledCheckers.Contains(checker);
        }

        internal void disableChecker(Checker checker) {
            if (!_disabledCheckers.Contains(checker))
                _disabledCheckers.Add(checker);
        }

		public bool moveApplied {
			get { return _movesApplied; }
		}

		private void clear() {
			_movesApplied = false;
			_heads = new List<PlayerColumn>();
            _disabledCheckers = new List<Checker>();
		}

        public void dispose() {
			clear();
			_player = null;
			_dices = null;
		}

	}

}