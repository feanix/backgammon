﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.src.scripts.Core.Interfaces {

    public interface IDispose {

        void dispose();

    }

}
