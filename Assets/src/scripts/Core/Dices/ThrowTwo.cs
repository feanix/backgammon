﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.src.scripts.Core.Dices {

    public class ThrowTwo : Throw {

        private uint _dice2;
		private bool _double;

        public ThrowTwo(Player player, uint dice1, uint dice2) : base(player, dice1) {
            _dice2 = dice2;
			_numberOfDices = 2;
            _moves = new List<uint>(2);
			_double = _dice2 == dice;
			if (dice1 != dice2) {
				_moves.Add(dice1);
				_moves.Add(dice2);
			} else {
				for (int i = 0; i < 4; i ++)
					_moves.Add(dice1);
			}
		}

		public override bool isDouble {
			get { return _double; }
		}

        public uint dice2 {
            get { return _dice2; }
        }

        override public uint total {
            get { return dice + dice2; }
        }

        public override string ToString() {
            return dice.ToString() + ", " + dice2.ToString();
        }

    }

}
