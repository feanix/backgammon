﻿using System;
using System.Collections.Generic;

namespace Assets.src.scripts.Core.Dices {

	public class Dice {

		public const int MINIMUM = 1;
		public const int MAXIMUM = 6;

        public static Throw throwOneDice(Player player) {
            List<uint> dices;
            Throw result;
            dices = throwDices(player, 1);
            result = new Throw(player, dices[0]);
            return result;
        }

        public static ThrowTwo throwTwoDices(Player player) {
            List<uint> dices;
            ThrowTwo result;
            dices = throwDices(player, 2);
            result = new ThrowTwo(player, dices[0], dices[1]);
            return result;
        }

        public static List<uint> throwDices(Player player, uint number) {
			if (number == 0)
				throw new Exception("Incorrect number of dices \"" + number + "\".");
			Random random = new Random();
			List<uint> result = new List<uint>();
			for (int i = 0; i < number; i++)
				result.Add((uint)random.Next(MINIMUM, MAXIMUM + 1));
			return result;
		}

	}

}
