﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.src.scripts.Core.Interfaces;

namespace Assets.src.scripts.Core.Dices {

    public class Throw : IDispose {

        private Player _player;
        private uint _dice;

        protected List<uint> _moves;
        protected uint _numberOfDices = 1;

        public Throw(Player player, uint dice) {
            clear();
            _player = player;
            _dice = dice;
            _moves = new List<uint>(1);
            _moves.Add(dice);
        }

		public virtual bool isDouble {
			get { return false; }
		}

        public Player player {
            get { return _player; }
        }

        public uint dice {
            get { return _dice; }
        }

        public virtual uint numberOfDices {
            get { return _numberOfDices; }
        }

        public virtual uint total {
            get { return _dice; }
        }

        public virtual List<uint> moves {
            get { return _moves; }
        }

        public override string ToString() {
            return dice.ToString();
        }

        public static bool operator >(Throw throw1, Throw throw2) {
            return throw1.total > throw2.total;
        }

        public static bool operator <(Throw throw1, Throw throw2) {
            return throw1.total < throw2.total;
        }

        private void clear() {

		}

		public void dispose() {
			clear();
			_player = null;
            _moves = null;
        }

    }

}
