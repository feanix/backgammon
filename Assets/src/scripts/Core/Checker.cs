﻿using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Interfaces;
using Assets.src.scripts.Core.Rules;

namespace Assets.src.scripts.Core {

    public abstract class Checker : IDispose {

        private Player _player;
        private CheckerType _type;
        private CheckersColumn _column;
        private Board _board;
        private Rule _rule;

        public Checker(Player player, CheckerType type) {
            clear();
            _player = player;
            _type = type;
            _board = player.board;
            _rule = _board.type.rules[type];
        }

        public PlayerColumn ownerColumn {
            get {
                return _player.getColumn(_column);
            }
        }

        public abstract bool banEnabled { get; }

        public Rule rule { get { return _rule; } }

		public int position {
			get {
				if (column == null)
					return -1;
				return _column.getPosition(this);
			}
		}

        public Player player {
		    get { return _player; }
	    }

	    public Board board {
		    get { return _board; }
	    }

        public CheckerType type {
            get { return _type; }
        }

        public CheckersColumn column {
            get { return _column; }
            internal set { _column = value; }
        }

        private void clear() {
            _column = null;
        }

        public void dispose() {
            clear();
            _player = null;
            _board = null;
            _rule = null;
        }

    }

}
