﻿using System;
using System.Collections.Generic;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Interfaces;

namespace Assets.src.scripts.Core {

    public class Board : IDispose {

        public const int LENGTH = 24;
        public const int OFFSET = 12;

		private Layout _type;
        private Dictionary<PlayerType, Player> _players;
        private List<CheckersColumn> _columns;
		private bool _tempMode;

		public Board(Layout type) {
			_type = type.instance;
            init();
        }

        private void init() {
            CheckersColumn tempColumn;
            clear();
            initPlayer(PlayerType.PLAYER_1);
            initPlayer(PlayerType.PLAYER_2);
            for (int j = 0; j < LENGTH; j++) {
                tempColumn = new CheckersColumn(j);
                _columns.Add(tempColumn);
                setPlayerColumn(tempColumn, PlayerType.PLAYER_1);
                setPlayerColumn(tempColumn, PlayerType.PLAYER_2);
            }
        }

		public bool ready {
			get {
				bool result = true;
				foreach (KeyValuePair<PlayerType, Player> keyValuePair in _players)
					result = result && keyValuePair.Value.ready;
				return result;
			}
		}

        private void setPlayerColumn(CheckersColumn column, PlayerType playerType) {
            Player player;
            player = _players[playerType];
            _players[playerType].setColumn(column);
        }

        private void initPlayer(PlayerType type) {
            _players.Add(type, new Player(this, type));
        }

        public Player getPlayer(PlayerType type) {
            if (!_players.ContainsKey(type))
                return null;
            return _players[type];
        }

	    internal static int getColumnByPlayer(Player player, int index) {
		    if (player.type == PlayerType.PLAYER_2)
			    index += OFFSET;
		    if (index >= LENGTH)
			    index -= LENGTH;
		    return index;
	    }

	    /*public void moveChecker(Player player, int fromColumn, int toColumn) {
		    fromColumn = getColumnByPlayer(player, fromColumn);
		    toColumn = getColumnByPlayer(player, toColumn);
			moveChecker(fromColumn, toColumn);
	    }

		private void moveChecker(int fromColumn, int toColumn) {
			CheckersColumn from;
			CheckersColumn to;
			from = getColumn(fromColumn);
			to = getColumn(toColumn);
			to.addChecker(from.topChecker);
		}*/

	    public CheckersColumn getColumn(Player player, int index) {
			if (player == null)
				return getColumn(index);
		    return getColumn(getColumnByPlayer(player, index));
	    }

		private CheckersColumn getColumn(int index) {
			CheckersColumn result;
			if ((index >= LENGTH) || (index < 0))
				throw new Exception("Column index \"" + index + "\" is out of the board.");
			result = _columns[index];
			if (result == null)
				throw new Exception("Column \"" + index + "\" has not been initialised.");
			return result;
		}

		public bool tempMode {
			get { return _tempMode; }
			set {
				if (_tempMode == value)
					return;
				_tempMode = value;
				foreach (CheckersColumn column in _columns)
					column.tempMode = _tempMode;
				foreach (KeyValuePair<PlayerType, Player> keyValuePair in _players)
					keyValuePair.Value.tempMode = _tempMode;
			}
		}

		public void applyChanges() {
			if (_tempMode == false)
				throw new Exception("Board is not in the TempMode.");
			_tempMode = false;
			foreach (CheckersColumn column in _columns)
				column.applyChanges();
			foreach (KeyValuePair<PlayerType, Player> keyValuePair in _players)
				keyValuePair.Value.applyChanges();
		}

		public Layout type {
			get { return _type; }
		}

		private void clear() {
            CheckersColumn tempColumn;
			_tempMode = false;
			disposeDictionary(_players);
            _players = new Dictionary<PlayerType, Player>();

            if (_columns != null) {
                for (int i = 0; i < _columns.Count; i++) {
                    tempColumn = _columns[i];
                    if (tempColumn == null)
                        continue;
                    tempColumn.dispose();
                    _columns[i] = null;
                }
            }
            _columns = new List<CheckersColumn>();
        }

		private void disposeDictionary<TValueType>(Dictionary<PlayerType, TValueType> dictionary) where TValueType : IDispose {
			if (dictionary == null)
				return;
			foreach (KeyValuePair<PlayerType, TValueType> keyValuePair in dictionary) {
				if (keyValuePair.Value == null)
					continue;
				keyValuePair.Value.dispose();
			}
		}

        public void dispose() {
            clear();
			if (_type != null) {
				_type.dispose();
				_type = null;
			}
            _players = null;
            _columns = null;
        }

    }

}