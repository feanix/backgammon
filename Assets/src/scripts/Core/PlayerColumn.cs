﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Interfaces;

namespace Assets.src.scripts.Core {

    public class PlayerColumn : IDispose {

        public const int HEAD_COLUMN = 0;

        private Board _board;
        private ColumnType _type;
        private Player _player;
        private CheckersColumn _column;
        private int _index;
        private bool _head;

        public PlayerColumn(Player player, ColumnType type, CheckersColumn column) {
            clear();
            _type = type;
            _player = player;
            _board = _player.board;
            _column = column;
            _index = column.getPlayerIndex(_player);
            _head = _index == HEAD_COLUMN;
        }

        public Player occupant {
            get {
                if (column.length == 0)
                    return null;
                return column.topChecker.player;
            }
        }

		internal bool tempMode {
			get { return _column.tempMode; }
			set { _column.tempMode = value; }
		}

		internal void applyChanges(bool playMode = true) {
			_column.applyChanges(playMode);
		}

        public bool head {
            get { return _head; }
        }

        public int index {
            get { return _index; }
        }

        public CheckersColumn column {
            get { return _column; }
        }

        public ColumnType type {
            get { return _type; }
        }

        public Board board {
            get { return _board; }
        }

        private void clear() {

        }

        public void dispose() {
            clear();
            _player = null;
            _column = null;
            _board = null;
        }

    }

}
