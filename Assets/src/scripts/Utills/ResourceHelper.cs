﻿using UnityEngine;
using System.Collections;

public class ResourceHelper : MonoBehaviour {

	public ColumnDisplay ColumnPrefab;
	public CheckerDisplay CheckerPrefab;

	[SerializeField] private NewGameWindow _newGameWindowPrefab;
	[SerializeField] private LoadingScreenWindow _loadingScreenWindowPrefab;
	[SerializeField] private MainGame _gameDisplayPrefab;

	[SerializeField] private Transform _topCanvas;

	public NewGameWindow newGameWindowPrefab { get { return _newGameWindowPrefab; } }
	public LoadingScreenWindow loadingScreenWindowPrefab { get { return _loadingScreenWindowPrefab; } }
	public MainGame gameDisplayPrefab { get { return _gameDisplayPrefab; } }

	public Transform topCanvas { get { return _topCanvas; } }

	public void Awake() {
		Catalogue.resourceHelper = this;
	}

}
