﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.src.scripts.API.VK;

namespace Assets.src.scripts.Utills.DTO {

    public class PlayerData {

		public string name = "NoName";
		public string firstName = "FirstName";
		public string lastName = "LastName";
		public string pictureURL_s = null;
		public string pictureURL_m = null;
		public string pictureURL_l = null;
		public string pictureURL = null;
		public int points = -1;
		public int games = -1;

		public PlayerData(string firstName, string lastName, int points, int games, string picture) {
			name = firstName + " " + lastName;
			this.firstName = firstName;
			this.lastName = lastName;
			pictureURL_s = picture;
			pictureURL_m = picture;
			pictureURL_l = picture;
			pictureURL = picture;
		}

		public PlayerData(VKUserDTO vkUser) {
			firstName = vkUser.first_name;
			lastName = vkUser.last_name;
			name = vkUser.first_name + " " + vkUser.last_name;
			pictureURL_s = vkUser.photo_200 != null ? vkUser.photo_200 : vkUser.photo_100;
			pictureURL_m = pictureURL_s;
			pictureURL_l = pictureURL_s;
			pictureURL = pictureURL_s;
		}

	}

}
