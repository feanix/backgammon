﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.src.scripts.Utills;

[RequireComponent(typeof(Text))]
public class LocalizedText : MonoBehaviour {

    [SerializeField]
    private string _reference;

    private string _translation;
    private Text _text;

    private void onLanguageChange() {
        updateText();
    }

	public string reference {
		set {
			_reference = value;
			updateText();
		}
		get { return _reference; }
	}

    private void updateText() {
        _translation = LocaleSingleton.getLocale(_reference);
		if (_translation == _reference)
			Debug.LogError("Error in LocalizedText \"" + gameObject.name + "\".");
        _text.text = _translation;
    }

    private void OnDestroy() {
        LocaleSingleton.onLanguageChange.RemoveListener(onLanguageChange);
    }

    void Start() {
        _text = gameObject.GetComponent<Text>();
        LocaleSingleton.onLanguageChange.AddListener(onLanguageChange);
        updateText();
    }

}
