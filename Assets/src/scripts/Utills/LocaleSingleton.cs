﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace Assets.src.scripts.Utills {

    public class LocaleSingleton {

        public static readonly UnityEvent onLanguageChange = new UnityEvent();
        private static SystemLanguage _language = SystemLanguage.English;

        private static Dictionary<string, Dictionary<SystemLanguage, string>> _locales = new Dictionary<string, Dictionary<SystemLanguage, string>> {
            {
                "ui.user.pointsTotal", new Dictionary<SystemLanguage, string> {
                    { SystemLanguage.English, "Your Points"},
                    { SystemLanguage.Russian, "Ваши Очки"},
                }
            }, {
                "ui.user.gamesTotal", new Dictionary<SystemLanguage, string> {
                    { SystemLanguage.English, "Your Games"},
                    { SystemLanguage.Russian, "Ваши Игры"},
                }
            }, {
                "ui.common.exit", new Dictionary<SystemLanguage, string> {
                    { SystemLanguage.English, "Exit"},
                    { SystemLanguage.Russian, "Выход"},
                }
            }, {
                "ui.common.exitGame", new Dictionary<SystemLanguage, string> {
                    { SystemLanguage.English, "EXIT GAME"},
                    { SystemLanguage.Russian, "ВЫХОД"},
                }
			}, {
				"ui.user.editProfile", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "edit profile"},
					{ SystemLanguage.Russian, "редактировать"},
				}
			}, {
				"ui.common.dynamic", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "DYNAMIC"},
					{ SystemLanguage.Russian, "ДИНАМИЧЕСКИЕ"},
				}
			}, {
				"ui.common.backgammon", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "BACKGAMMON"},
					{ SystemLanguage.Russian, "НАРДЫ"},
				}
			}, {
				"ui.windows.newGame", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "NEW GAME"},
					{ SystemLanguage.Russian, "НОВАЯ ИГРА"},
				}
			}, {
				"ui.windows.classicGame", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "CLASSIC"},
					{ SystemLanguage.Russian, "КЛАССИКА"},
				}
			}, {
				"ui.windows.oneDragonGame", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "ONE DRAGON"},
					{ SystemLanguage.Russian, "ОДИН ДРАКОН"},
				}
			}, {
				"ui.windows.selectNewGame", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "Select the type of new game."},
					{ SystemLanguage.Russian, "Выберите тип новой игры."},
				}
			}, {
				"ui.game.userPoints", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "Points"},
					{ SystemLanguage.Russian, "Очки"},
				}
			}, {
				"ui.game.userGames", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "Games"},
					{ SystemLanguage.Russian, "Игры"},
				}
			}, {
				"ui.game.Roll", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "Roll:"},
					{ SystemLanguage.Russian, "Зары:"},
				}
			}, {
				"ui.game.PromptsCaption", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "Enable additional prompts"},
					{ SystemLanguage.Russian, "Включить дополнительные подсказки"},
				}
			}, {
				"ui.game.PromptsStateOFF", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "NO"},
					{ SystemLanguage.Russian, "НЕТ"},
				}
			}, {
				"ui.game.PromptsStateON", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "YES"},
					{ SystemLanguage.Russian, "ДА"},
				}
			}, {
				"ui.game.UndoButton", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "UNDO"},
					{ SystemLanguage.Russian, "ОТМЕНА"},
				}
			}, {
				"ui.game.WinCaption", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "WIN"},
					{ SystemLanguage.Russian, "ПОБЕДА"},
				}
			}, {
				"ui.game.WinNote", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "EXCELLENT RESULT"},
					{ SystemLanguage.Russian, "ОТЛИЧНЫЙ РЕЗУЛЬТАТ"},
				}
			}, {
				"ui.game.LoseCaption", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "DEFEAT"},
					{ SystemLanguage.Russian, "ПОРАЖЕНИЕ"},
				}
			}, {
				"ui.game.LoseNote", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "GIVE IT ANOTHER TRY"},
					{ SystemLanguage.Russian, "ПОПРОБУЙ ЕЩЁ РАЗ"},
				}
			}, {
				"ui.game.RollTheDice", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "ROLL THE DICE"},
					{ SystemLanguage.Russian, "БРОСИТЬ ЗАРЫ"},
				}
			}, {
				"ui.windows.moveType", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "DOUBLE"},
					{ SystemLanguage.Russian, "КУШ"},
				}
			}, {
				"ui.windows.moveTypeNote", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "You can retrieve checkers from the bar."},
					{ SystemLanguage.Russian, "Можно снять шашки с бара."},
				}
			}, {
				"ui.windows.move", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "CANCEL"},
					{ SystemLanguage.Russian, "ОТМЕНА"},
				}
			}, {
				"ui.windows.retrieve", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "RETRIEVE"},
					{ SystemLanguage.Russian, "СНЯТЬ"},
				}
			}, {
				"ui.loading.loading", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "LOADING"},
					{ SystemLanguage.Russian, "ЗАГРУЗКА"},
				}
			}, {
				"ui.loading.loadingNote1", new Dictionary<SystemLanguage, string> {
					{ SystemLanguage.English, "Loading user profile."},
					{ SystemLanguage.Russian, "Загрузка данных пользователя."},
				}
			}
		};

        public static SystemLanguage language {
            get { return _language; }
            set {
                if (_language == value)
                    return;
                _language = value;
                onLanguageChange.Invoke();
            }
        }

        public static void addLocale(string reference, SystemLanguage language, string translation) {
            Dictionary<SystemLanguage, string> locale;
            if (!_locales.ContainsKey(reference))
                locale = new Dictionary<SystemLanguage, string>();
            else
                locale = _locales[reference];
            if (!locale.ContainsKey(language))
                locale.Add(language, translation);
            else
                locale[language] = translation;
        }

        public static string getLocale(string reference) {
            Dictionary<SystemLanguage, string> locale;
			if (reference.Length == 0) {
				Debug.LogError("Reference is not set.");
				return reference;
			}
			if (!_locales.ContainsKey(reference)) {
                Debug.LogError("Locale " + reference + " does not exist.");
                return reference;
            }
            locale = _locales[reference];
            if (!locale.ContainsKey(_language)) {
                foreach (KeyValuePair<SystemLanguage, string> keyValuePair in locale) {
                    Debug.LogError("Locale " + reference + " is not translated to " + _language.ToString() + ".");
                    return keyValuePair.Value;
                }
            } else {
                return locale[_language];
            }

            Debug.LogError("Locale " + reference + " does not exist.");
            return reference;
        }

    }

}
