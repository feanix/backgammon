﻿using System.Collections.Generic;
using Assets.src.scripts.Core.Analysis;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Managers;
using Assets.src.scripts.Utills.DTO;
using UnityEngine;
using UnityEngine.Events;

public class Catalogue {

    public readonly static List<CheckerMove> FREE_MODE = new List<CheckerMove>();
	private static Stage _stage = new GameObject().AddComponent<Stage>();

	public const float MOVE_SPEED = 1;
	public const string APP_ID_VK = "6744746";

	private static MonoBehaviour _monoBehaviour;

	public static readonly UnityEvent onAppUserChange = new UnityEvent();
	public static readonly UnityEvent onCurrentPlayerChange = new UnityEvent();
	private static PlayerManager _appUser = new PlayerManager();
	private static PlayerManager _opponent = new PlayerManager();
	private static PlayerManager _currentPlayer = new PlayerManager();
	public static PlayerManager appUser {
		get { return _appUser; }
		set {
			_appUser = value;
			onAppUserChange.Invoke();
		}
	}
	public static PlayerManager currentPlayer {
		get { return _currentPlayer; }
	}

	public static Stage stage { get { return _stage; } }

	public static PlayerType currentPlayerType {
		set {
			if (value == PlayerType.PLAYER_1)
				_currentPlayer = _appUser;
			else {
				_currentPlayer = _opponent;
			}
			onCurrentPlayerChange.Invoke();
		}
	}

	private static ResourceHelper _resourceHelper;
	public static ResourceHelper resourceHelper {
		get { return _resourceHelper; }
		set { _resourceHelper = value; }
	}

	public static void init(MonoBehaviour monoBehaviour) {
		_monoBehaviour = monoBehaviour;
#if VK_WEBGL_RELEASE
		VKWEBWrapper.init(userInfoHandler);
#endif
#if VK_STANDALONE_RELEASE
		VKStandaloneWrapper.init();
		VKStandaloneWrapper.getUserInfo(userInfoHandler);
#endif
	}

	private static void userInfoHandler(PlayerData user) {
		appUser.init(user);
		Stage.loadingScreen.progress = 1;
		Stage.loadingComplete();
	}

	public static void StartCoroutine(System.Collections.IEnumerator routine) {
		_monoBehaviour.StartCoroutine(routine);
	}

}
