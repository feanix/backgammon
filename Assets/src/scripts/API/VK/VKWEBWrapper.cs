﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Assets.src.scripts.API.VK;
using Assets.src.scripts.Utills.DTO;

public class VKWEBWrapper : MonoBehaviour {

	[DllImport("__Internal")]
	private static extern void VKInit();

	[DllImport("__Internal")]
	public static extern string  VKUserInfo();

	private static VKWEBWrapper _instance;

	private static Action<PlayerData> _initCallback = null;

	public static void init(Action<PlayerData> callback) {
		_initCallback = callback;
		_instance = new GameObject().AddComponent<VKWEBWrapper>();
		_instance.gameObject.name = "VKWEBWrapper";
		if (Application.isEditor)
			_instance.VKUserInfoCallback("[{ \"uid\":25746196,\"first_name\":\"Izmail\",\"last_name\":\"Ostengang\",\"photo_100\":\"https://pp.userapi.com/c11491/u25746196/d_0d46fc2c.jpg?ava=1\"}]");
		else
			VKInit();
	}

	public void VKInitCallback() {
		Debug.Log("VKInitCallback");
		VKUserInfo();
	}

	public void VKUserInfoCallback(string responseString) {
		Debug.Log("VKUserInfoCallback: " + responseString);
		VKUserDTO user;
		VKResponse<List<VKUserDTO>> response;
		responseString = "{\"response\":" + responseString + "}";
		response = JsonUtility.FromJson<VKResponse<List<VKUserDTO>>>(responseString);
		user = response.response[0];
		PlayerData playerData;
		Debug.Log(user);
		playerData = new PlayerData(user);
		_initCallback(playerData);
	}

}