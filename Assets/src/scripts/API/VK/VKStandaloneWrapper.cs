﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.src.scripts.API.VK;
using Assets.src.scripts.Utills.DTO;
using UnityEngine;

public class VKStandaloneWrapper {

	private const string TEST_URL =
		"https://vk.dingammon.com/?api_url=https://api.vk.com/api.php&api_id=6744746&api_settings=1&viewer_id=25746196&viewer_type=2&sid=c83dcfd9789073925a7a28e0a4e4a0861609b9c71c8d78792dddf89856b9c3835ba8eedc51a31efcb6bc5&secret=82d5d6cbdb&access_token=992ab6d1b267fe4fda7864def8c4462a69d957747901562aabbc19317fe191ba6fdab51ec4f62de126201&user_id=25746196&group_id=0&is_app_user=1&auth_key=b87f93ec2a60b916cd2db278ad22fa10&language=3&parent_language=3&is_secure=1&stats_hash=86a2a7660fede4b433&ads_app_id=6744746_80ff8e1fe56c8206e0&referrer=unknown&lc_name=536ed217&platform=web&hash=";

	private static Dictionary<string, string> _parameters = new Dictionary<string, string>();
	private static VKStandaloneWrapper _instance = new VKStandaloneWrapper();

	private VKStandaloneWrapper() {
	}

	public static void init() {
		string parameters = "";
		string[] urlParts;
		string[] keyValuePair;
		_parameters = new Dictionary<string, string>();
		int pm = Application.absoluteURL.IndexOf("?");
		if (pm != -1) {
			parameters = Application.absoluteURL.Split('?')[1];
		} else {
#if !RELEASE_BUILD
			parameters = TEST_URL.Split('?')[1];
#endif
		}
		urlParts = parameters.Split('&');
		foreach (string urlPart in urlParts) {
			keyValuePair = urlPart.Split('=');
			_parameters.Add(keyValuePair[0], keyValuePair[1]);
		}
	}

	public static string getParameter(string name) {
		if (_parameters.ContainsKey(name))
			return _parameters[name];
		return null;
	}

	public static string appID { get { return getParameter("api_id"); } }
	public static string APIURL { get { return "https://api.vk.com/method/"; } }
	public static string APIVersion { get { return "5.21"; } }
	public static string userID { get { return getParameter("viewer_id"); } }
	public static string sid { get { return getParameter("sid"); } }
	public static string secret { get { return getParameter("secret"); } }
	public static string token { get { return getParameter("access_token"); } }
	public static string auth { get { return getParameter("auth_key"); } }

	private static VKStandaloneWrapper instance {
		get {
			if (_instance == null)
				_instance = new VKStandaloneWrapper();
			return _instance;
		}
	}

	public static void getUserInfo(Action<PlayerData> callback) {
		Catalogue.StartCoroutine(instance.getUserInfoI(callback));
	}

	private IEnumerator getUserInfoI(Action<PlayerData> callback) {
		string request;
		string result;
		VKUserDTO user;
		VKResponse<List<VKUserDTO>> response;
		request = composeRequest("users.get", new Dictionary<string, string> {{"fields", "photo_200, photo_100"}});
		WWW www = new WWW(request);
		yield return www;
		//result = www.bytes.ToString();
		result = www.text;
		Debug.Log(result);
		response = JsonUtility.FromJson<VKResponse<List<VKUserDTO>>>(result);
		user = response.response[0];
		PlayerData playerData;
		Debug.Log(user);
		playerData = new PlayerData(user);
		callback(playerData);
	}

	private string composeRequest(string method, Dictionary<string, string> options = null) {
		//https://api.vk.com/method/users.get?fields=photo_200,photo_100&uid=25746196&api_id=9744746&access_token=992ab6d1b267fe4fda7864def8c4462a69d957747901562aabbc19317fe191ba6fdab51ec4f62de126201&v=5.21
		string result = APIURL;
		result = result + method + "?uid=" + userID + "&access_token=" + token + "&api_id=" + appID + "&v=" + APIVersion;
		if (options != null) {
			foreach (KeyValuePair<string, string> option in options) {
				result = result + "&" + option.Key + "=" + option.Value;
			}
		}
		return result;
	}

}
