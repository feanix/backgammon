﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.src.scripts.API.VK {

	[Serializable]
	public class VKResponse<T> {

		[SerializeField] public T response;

	}

}
