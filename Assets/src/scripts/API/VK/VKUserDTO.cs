﻿using System;
using UnityEngine;

namespace Assets.src.scripts.API.VK {

	[Serializable]
	public class VKUserDTO {

		[SerializeField] public string id = null;
		[SerializeField] public int uid = 0;
		[SerializeField] public string first_name;
		[SerializeField] public string last_name;
		[SerializeField] public string photo_100;
		[SerializeField] public string photo_200;

	}

}
