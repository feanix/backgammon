﻿using System;
using System.Collections.Generic;
using Assets.src.scripts.Net.DTO;
using UnityEngine;

namespace Assets.src.scripts.Net {

	[Serializable]
	public class NetResponse {

		[SerializeField] public NetMethod method;
		[SerializeField] public NetStatus status;
		[SerializeField] public NetResponseDTO data;
		[SerializeField] public MessageDTO msg;
		[SerializeField] public AuthDTO auth;

	}

}
