﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Net.DTO;
using Assets.src.scripts.Net.DTO.Requests;
using Assets.src.scripts.Net.interfaces;
using JetBrains.Annotations;
using Random = UnityEngine.Random;

namespace Assets.src.scripts.Net {

	public class NetBridgeOffline : INetBridge {

		private const string APP_ID = "4526576grrf";

		private static string _token = null;
		private static UserInfoDTO _userInfo = null;
		private static UserInfoDTO _opponentInfo = null;
		private static BoardDTO _userBoard = null;
		private static BoardDTO _opponentBoard = null;

		public IEnumerator externalCall(NetRequest<NetRequestDTO> request, Action<NetResponse> callback) {
			yield return null;
			execute(request, callback);
		}

		private void execute(NetRequest<NetRequestDTO> request, Action<NetResponse> callback) {
			NetResponse response;
			NetResponseDTO responseDTO = null;
			AuthDTO auth;
			switch (request.method) {
				case NetMethod.SignIn:
					SignInRequest signInRequest = (SignInRequest)request.data;
					responseDTO = signIn(signInRequest);
				break;
				case NetMethod.UserDataGet:
					UserDataGetRequest userDataRequest = (UserDataGetRequest)request.data;
					responseDTO = userDataGet(userDataRequest);
				break;
				case NetMethod.BoardsGet:
					BoardsGetRequest boardsGetRequest = (BoardsGetRequest) request.data;
					responseDTO = boardsGet(boardsGetRequest);
				break;
			}

			response = new NetResponse();
			response.method = request.method;
			auth = new AuthDTO();
			auth.appID = APP_ID;
			auth.n = Random.Range(100, 10000);
			auth.timeStamp = DateTime.Now.Ticks/TimeSpan.TicksPerMillisecond;
			auth.token = _token;
			auth.userID = _userInfo.id;
			response.status = NetStatus.success;
			response.data = responseDTO;
			response.auth = auth;
			callback(response);
		}

		private NetResponseDTO boardsGet(BoardsGetRequest request) {
			NetResponseDTO response;
			response = new NetResponseDTO();
			response.boards = new List<BoardDTO>();
			response.boards.Add(userBoard);
			response.boards.Add(opponentBoard);
			return response;
		}

		private NetResponseDTO userDataGet(UserDataGetRequest request) {
			NetResponseDTO response;
			response = new NetResponseDTO();
			response.online = true;
			if (request.userID == _userInfo.id) {
				response.userInfo = userInfo;
				response.board = userBoard;
			} else {
				response.userInfo = opponentInfo;
				response.board = opponentBoard;
			}
			response.inventory = new List<ItemDTO>();
			return response;
		}

		private NetResponseDTO signIn(SignInRequest request) {
			NetResponseDTO response;
			AuthDataDTO authData;
			_userInfo = new UserInfoDTO();
			_userBoard = new BoardDataDTO();
			prepareUser(_userInfo, _userBoard);
			_userInfo.name = "Izmail Ostengang";
			_userInfo.picture = "https://pp.userapi.com/c11491/u25746196/d_0d46fc2c.jpg?ava=1";

			_opponentInfo = new UserInfoDTO();
			_opponentBoard = new BoardDTO();
			prepareUser(_opponentInfo, _opponentBoard);
			_opponentInfo.name = "Ivan Pupkin";
			_opponentInfo.picture = "https://pp.userapi.com/c605728/v605728795/18a5/LRXt726YXNY.jpg?ava=1";

			_token = MD5.Create(Random.Range(0f, 1f).ToString()).ToString();

			response = new NetResponseDTO();
			authData = new AuthDataDTO();
			authData.token = _token;
			authData.userID = _userInfo.id;
			response.authData = authData;

			return response;
		}

		private void prepareUser([NotNull]UserInfoDTO user, [NotNull]BoardDTO board) {
			user.game = null;
			user.gamesTotal = Random.Range(10, 1000);
			user.score = Random.Range(100, 10000);
			user.id = MD5.Create(Random.Range(0f, 1f).ToString()).ToString();
			user.inGame = true;
			user.rank = Random.Range(1, 5);
			board.id = _userInfo.id;
			board.active = false;
			board.bet = Random.Range(1, 4)*25;
			board.currency = 0;
			board.layout = GameType.BASIC.ToString();
			board.playerTime = 0;
			board.publicBoard = true;
			board.secure = false;
			board.stepTime = 0;
		}

		private UserInfoDTO userInfo {
			get { return _userInfo.clone; }
		}

		private UserInfoDTO opponentInfo {
			get { return _opponentInfo.clone; }
		}

		private BoardDTO userBoard {
			get { return _userBoard.clone; }
		}

		private BoardDTO opponentBoard {
			get { return _opponentBoard.clone; }
		}

	}

}