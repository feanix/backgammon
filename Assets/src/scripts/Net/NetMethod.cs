﻿namespace Assets.src.scripts.Net {

	public enum NetMethod {
		SignIn,
		UserDataGet,
		BoardsGet,
		BoardOpen,
		BoardLeave,
		UserInfo,
		BoardJoin,
		BoardUpdate,
		RollTheDice,
		CheckerMove,
		BoardUnDo,
		NextPlayer
	}

}
