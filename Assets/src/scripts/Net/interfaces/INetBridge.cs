﻿using System;
using System.Collections;
using Assets.src.scripts.Net.DTO;
using Assets.src.scripts.Net.DTO.Requests;

namespace Assets.src.scripts.Net.interfaces {

	public interface INetBridge {

		IEnumerator externalCall(NetRequest<NetRequestDTO> request, Action<NetResponse> callback);

	}

}
