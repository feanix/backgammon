﻿using System;
using Assets.src.scripts.Net.DTO;
using Assets.src.scripts.Net.DTO.Requests;
using UnityEngine;

namespace Assets.src.scripts.Net {

	[Serializable]
	public class NetRequest<T> where T: NetRequestDTO {

		[SerializeField]
		public NetMethod method {
			get { return data.method; }
			set { }
		}
		[SerializeField] public T data;
		[SerializeField] public AuthDTO auth;

	}

}
