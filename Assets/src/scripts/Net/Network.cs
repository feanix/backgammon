﻿using System;
using UnityEngine;
using System.Collections;
using Assets.src.scripts.Net.DTO;
using Assets.src.scripts.Net.DTO.Requests;
using Assets.src.scripts.Net.interfaces;

namespace Assets.src.scripts.Net {

	public class Network {

		private static MonoBehaviour _enumerator = new GameObject().AddComponent<MonoBehaviour>();
		private static INetBridge _netBridge;
		private static Network _instance = new Network();

		private Network() {
#if OFFLINE_MODE
			_netBridge = 
#else

#endif
		}

		private static void call(NetRequest<NetRequestDTO> request, Action<NetResponse> callback) {
			_enumerator.StartCoroutine(_netBridge.externalCall(request, callback));
		}

	}

}

