﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class ItemDTO {

		[SerializeField] public int id;
		[SerializeField] public int amount;
		[SerializeField] public int level;

	}

}
