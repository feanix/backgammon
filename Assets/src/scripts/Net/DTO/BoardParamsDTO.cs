﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class BoardParamsDTO : BoardBaseDTO {

		[SerializeField] public string password;

	}

}
