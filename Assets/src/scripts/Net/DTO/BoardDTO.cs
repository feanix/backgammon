﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class BoardDTO :BoardBaseDTO {

		[SerializeField] public string id;
		[SerializeField] public bool active;

		public BoardDTO clone {
			get { return clone<BoardDTO>(); }
		}

	}

}
