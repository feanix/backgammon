﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class UserDataDTO {

		[SerializeField] public string userName;
		[SerializeField] public int age;
		[SerializeField] public string gender;
		[SerializeField] public string picture;

	}

}
