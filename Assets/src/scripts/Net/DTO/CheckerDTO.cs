﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class CheckerDTO {

		[SerializeField] public int number;
		[SerializeField] public int id;
		[SerializeField] public int owner;

	}

}
