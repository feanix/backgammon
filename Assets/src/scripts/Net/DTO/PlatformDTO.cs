﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class PlatformDTO {

		[SerializeField] public string foreignUserID;
		[SerializeField] public string foreignAppID;
		[SerializeField] public string type;

	}

}
