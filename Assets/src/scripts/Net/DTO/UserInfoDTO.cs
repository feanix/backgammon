﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class UserInfoDTO : ClonableDTO {

		[SerializeField] public string id;
		[SerializeField] public string name;
		[SerializeField] public string picture;
		[SerializeField] public int score;
		[SerializeField] public int gamesTotal;
		[SerializeField] public int rank;
		[SerializeField] public string game;
		[SerializeField] public bool inGame;

		public UserInfoDTO clone {
			get { return clone<UserInfoDTO>(); }
		}

	}

}
