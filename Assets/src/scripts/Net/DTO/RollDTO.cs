﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class RollDTO {

		[SerializeField] public int player;
		[SerializeField] public DiceDTO dice;
		[SerializeField] public int time;
		[SerializeField] public int duration;
		[SerializeField] public DiceDTO lastRoll1;
		[SerializeField] public DiceDTO lastRoll2;
		[SerializeField] public int playerTime1;
		[SerializeField] public int playerTime2;

	}

}
