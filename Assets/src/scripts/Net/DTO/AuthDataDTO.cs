﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class AuthDataDTO {

		[SerializeField] public string userID;
		[SerializeField] public string token;
		[SerializeField] public string API_URL;
		[SerializeField] public string staticData;

	}

}
