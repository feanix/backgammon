﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class BoardBaseDTO : ClonableDTO {

		[SerializeField] public string layout;
		[SerializeField] public int currency;
		[SerializeField] public int bet;
		[SerializeField] public bool secure;
		[SerializeField] public bool publicBoard;
		[SerializeField] public int playerTime;
		[SerializeField] public int stepTime;

	}

}
