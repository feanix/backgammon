﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class AuthDTO {

		[SerializeField] public string appID;
		[SerializeField] public string userID;
		[SerializeField] public string token;
		[SerializeField] public long timeStamp;
		[SerializeField] public int n;

	}

}
