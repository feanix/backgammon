﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	public class ClonableDTO {

		public ClonableDTO() { }

		public T clone<T>() where T : ClonableDTO, new() {
			T result;
			string data;
			result = new T();
			data = JsonUtility.ToJson(this);
			result = JsonUtility.FromJson<T>(data);
			return result;
		}

	}

}