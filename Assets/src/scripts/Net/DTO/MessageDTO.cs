﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class MessageDTO {

		[SerializeField] public string title;
		[SerializeField] public string text;
		[SerializeField] public string block;

	}

}
