﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class PlayersDTO {

		[SerializeField] public string player1;
		[SerializeField] public string player2;
		[SerializeField] public List<string> spectators;

	}

}
