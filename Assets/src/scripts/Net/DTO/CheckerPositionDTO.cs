﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class CheckerPositionDTO {

		[SerializeField] public int number;
		[SerializeField] public int column;
		[SerializeField] public int index;

	}

}
