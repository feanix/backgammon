﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class BoardFilterDTO {

		[SerializeField] public int rank;
		[SerializeField] public int currency;
		[SerializeField] public int betMin;
		[SerializeField] public int betMax;
		[SerializeField] public int number;
		[SerializeField] public int offset;
		[SerializeField] public bool friends;

	}

}
