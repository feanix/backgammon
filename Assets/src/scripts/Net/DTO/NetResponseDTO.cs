﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class NetResponseDTO {

		//SignIn
		[SerializeField] public AuthDataDTO authData;

		//UserDataGet
		[SerializeField] public bool online;
		[SerializeField] public UserInfoDTO userInfo;
		[SerializeField] public List<ItemDTO> inventory;
		//UserDataGet BoardLeave
		[SerializeField] public BoardDTO board;

		//BoardsGet
		[SerializeField] public List<BoardDTO> boards;

		//UserInfo
		[SerializeField] public List<UserInfoDTO> usersData;

		//BoardUpdate
		[SerializeField] public BoardDataDTO boardData;
		[SerializeField] public List<ItemDTO> reward;

	}

}
