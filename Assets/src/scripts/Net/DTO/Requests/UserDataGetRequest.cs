﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public abstract class UserDataGetRequest : NetRequestDTO {

		public UserDataGetRequest() : base(NetMethod.UserDataGet) { }

		[SerializeField] public string userID;

	}

}
