﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public abstract class SignInRequest : NetRequestDTO {

		public SignInRequest() : base(NetMethod.SignIn) { }

		[SerializeField] public PlatformDTO platform;
		[SerializeField] public UserDataDTO userData;
		[SerializeField] public Dictionary<string, string> platformData;

	}

}
