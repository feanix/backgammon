﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public class CheckerMoveRequest : NetRequestDTO {

		public CheckerMoveRequest() : base(NetMethod.CheckerMove) { }

		[SerializeField] public string boardId;
		[SerializeField] public List<CheckerPositionDTO> checkers;

	}

}
