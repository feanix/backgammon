﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public abstract class UserInfoRequest : NetRequestDTO {

		public UserInfoRequest() : base(NetMethod.UserInfo) { }

		[SerializeField] public List<string> users;

	}

}
