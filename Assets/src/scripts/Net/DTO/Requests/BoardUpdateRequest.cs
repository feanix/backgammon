﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public class BoardUpdateRequest : NetRequestDTO {

		public BoardUpdateRequest() : base(NetMethod.BoardUpdate) { }

		[SerializeField] public string boardId;

	}

}
