﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public abstract class BoardsGetRequest : NetRequestDTO {

		public BoardsGetRequest() : base(NetMethod.BoardsGet) { }

		[SerializeField] public BoardFilterDTO boardFilter;

	}

}
