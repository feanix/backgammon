﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public class NextPlayerRequest : NetRequestDTO {

		public NextPlayerRequest() : base(NetMethod.NextPlayer) { }

		[SerializeField] public string boardId;

	}

}
