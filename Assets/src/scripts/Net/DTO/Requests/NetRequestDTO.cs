﻿using System;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public abstract class NetRequestDTO {

		private NetMethod _method;

		public NetRequestDTO(NetMethod method) {
			_method = method;
		}

		public NetMethod method {
			get { return _method; }
		}

	}

}
