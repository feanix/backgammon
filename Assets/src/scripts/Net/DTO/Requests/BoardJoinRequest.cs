﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public class BoardJoinRequest : NetRequestDTO {

		public BoardJoinRequest() : base(NetMethod.BoardJoin) { }

		[SerializeField] public BoardDataDTO boardData;

	}

}
