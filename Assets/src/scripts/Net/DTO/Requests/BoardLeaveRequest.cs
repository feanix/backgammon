﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public class BoardLeaveRequest : NetRequestDTO {

		public BoardLeaveRequest(): base(NetMethod.BoardLeave) { }

		[SerializeField] public string boardID;

	}

}
