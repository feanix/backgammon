﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public class BoardOpenRequest : NetRequestDTO {

		public BoardOpenRequest() : base(NetMethod.BoardOpen) { }

		[SerializeField] public BoardParamsDTO boardParams;

	}

}
