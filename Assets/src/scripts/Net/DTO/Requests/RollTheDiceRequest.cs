﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public class RollTheDiceRequest : NetRequestDTO {

		public RollTheDiceRequest() : base(NetMethod.RollTheDice) { }

		[SerializeField] public string boardId;

	}

}
