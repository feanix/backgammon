﻿using System;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO.Requests {

	[Serializable]
	public class BoardUnDoRequest : NetRequestDTO {

		public BoardUnDoRequest() : base(NetMethod.BoardUnDo) { }

		[SerializeField] public string boardId;

	}

}
