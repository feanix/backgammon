﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.src.scripts.Net.DTO {

	[Serializable]
	public class BoardDataDTO :BoardDTO {

		[SerializeField] public int startTime;
		[SerializeField] public int duration;
		[SerializeField] public string status;
		[SerializeField] public List<CheckerDTO> checkers;
		[SerializeField] public List<CheckerPositionDTO> map;
		[SerializeField] public RollDTO roll;
		[SerializeField] public PlayersDTO players;

	}

}
