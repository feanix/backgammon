﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Assets.src.scripts.Core;
using Assets.src.scripts.Core.Constants;
using Assets.src.scripts.Core.Dices;
using System.Collections.Generic;

public class MainUI : MonoBehaviour {

	[SerializeField] private Text currentPlayer;
	[SerializeField] private Button _rollDiceButton;
	[SerializeField] private Transform _rollDicePopup;
	[SerializeField] private Transform _gameUI;
	[SerializeField] private Button _undoButton;
	[SerializeField] private MainGame game;
	[SerializeField] private PlayerUI _player1UI;
	[SerializeField] private PlayerUI _player2UI;
	[SerializeField] private GameObject _moveTypeContainer;
	[SerializeField] private GameEndWindow _winWindow;
	[SerializeField] private DiceDisplay _dicePreview;

	private Dictionary<PlayerType, PlayerUI> _playersUI;

	private Player _throwPlayer;
	private Throw _doubleRoll;

	private void startGame(Layout type) {
		Debug.Log("Start game: " + type.name);
		gameStarted(true);
		game.init(type);
	}

	public void exitGame() {
		game.exitGame();
		gameStarted(false);
		_doubleRoll = null;
		_throwPlayer = null;
		_winWindow.hide();
	}

    private uint _numberOfDices;
    private Throw throwResult;
	private void ThrowDices() {
		Throw dice = null;

		Action throwUICallback = () => {
			Debug.Log("throwUICallback");
			game.throwDices(dice);
		};

		throwEnabled = false;
		switch (_numberOfDices) {
			case 1:
				dice = Dice.throwOneDice(_throwPlayer);
				break;
			case 2:
				dice = Dice.throwTwoDices(_throwPlayer);
				break;
		}
		foreach(KeyValuePair<PlayerType, PlayerUI> keyValuePair in _playersUI) {
			if (keyValuePair.Key == _throwPlayer.type) {
				_rollDicePopup.gameObject.SetActive(false);
				keyValuePair.Value.visible = true;
				keyValuePair.Value.dice.throwDice(dice, throwUICallback);
				_dicePreview.throwDice(dice, null);
			} else {
				keyValuePair.Value.visible = false;
			}
		}

	}

	private bool moveTypeEnabled {
		set { _moveTypeContainer.SetActive(value); }
	}

	private void onDoubleHandler(Throw roll) {
		_doubleRoll = roll;
		moveTypeEnabled = true;
	}

	public void retrieveHandler() {
		moveTypeEnabled = false;
		game.retrieve(_doubleRoll);
		_doubleRoll = null;
	}

	public void moveHandler() {
		moveTypeEnabled = false;
		game.throwDices(_doubleRoll, true);
		_doubleRoll = null;
	}

	private void undo() {
		game.undo();
	}

	public void gameStarted(bool value) {
		NewGameWindow newGameWindow;
		if (!value) {
			newGameWindow = WindowsSingleton.addWindow(Catalogue.resourceHelper.newGameWindowPrefab);
			newGameWindow.init(startGame);
		}

		UserInfoWindow.setGameStarted(value);
		_gameUI.gameObject.SetActive(value);
		if (!value) {
			_undoButton.gameObject.SetActive(false);
			_rollDicePopup.gameObject.SetActive(false);
			foreach (KeyValuePair<PlayerType, PlayerUI> keyValuePair in _playersUI) {
				keyValuePair.Value.dice.hide();
			}
		}
	}

	private void throwRequiredHandler(Player player, uint number) {
        _numberOfDices = number;
        _throwPlayer = player;
		throwEnabled = true;
	}

	private void onUndoStatusHandler(bool status) {
		_undoButton.gameObject.SetActive(status);
	}

	private void onWinHandler(Player winner, uint arg) {
		_winWindow.show(winner.type == PlayerType.PLAYER_1);
	}

	void Start() {
		_playersUI = new Dictionary<PlayerType, PlayerUI>();
		_playersUI.Add(PlayerType.PLAYER_1, _player1UI);
		_playersUI.Add(PlayerType.PLAYER_2, _player2UI);
		foreach (KeyValuePair<PlayerType, PlayerUI> keyValuePair in _playersUI) {
			keyValuePair.Value.playerName = keyValuePair.Key.ToString();
		}
		gameStarted(false);
		_rollDiceButton.onClick.AddListener(ThrowDices);
		_undoButton.onClick.AddListener(undo);
		game.onThrowRequired.AddListener(throwRequiredHandler);
        game.onCurrentPlayer.AddListener(currentPlayerHandler);
		game.onUndoStatus.AddListener(onUndoStatusHandler);
		game.onDouble.AddListener(onDoubleHandler);
		game.onWin.AddListener(onWinHandler);
	}

	public void Awake() {
		_undoButton.gameObject.SetActive(false);
		_moveTypeContainer.SetActive(false);
	}

	private void currentPlayerHandler(Player player, uint number) {
        currentPlayer.text = player.name;
		foreach (KeyValuePair<PlayerType, PlayerUI> keyValuePair in _playersUI) {
			keyValuePair.Value.visible = keyValuePair.Key == player.type;
		}
	}

	private bool throwEnabled {
		set {
			_rollDicePopup.gameObject.SetActive(value);
		}
	}

}
