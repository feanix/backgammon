﻿using UnityEngine.Events;
using Assets.src.scripts.Core.Analysis;

[System.Serializable]
public class MoveEvent : UnityEvent<CheckerMove> {

}
