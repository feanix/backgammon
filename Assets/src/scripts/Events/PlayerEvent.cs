﻿using UnityEngine.Events;
using Assets.src.scripts.Core;

[System.Serializable]
public class PlayerEvent : UnityEvent<Player, uint> {

}
