﻿using UnityEngine.Events;
using Assets.src.scripts.Core.Dices;

[System.Serializable]
public class RollEvent : UnityEvent<Throw> {

}
