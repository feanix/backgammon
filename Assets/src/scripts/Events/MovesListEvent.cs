﻿using System.Collections.Generic;
using UnityEngine.Events;
using Assets.src.scripts.Core;
using Assets.src.scripts.Core.Analysis;

[System.Serializable]
public class MovesListEvent : UnityEvent<Player, List<CheckerMove>> {

}
